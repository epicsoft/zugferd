# ZUGFeRD

This library provides Java classes for generating ZUGFeRD / Factur-X XML files.

ZUGFeRD schemes from https://www.ferd-net.de/ are used.

ZUGFeRD EXTENDED scheme is used for generation.

The [PDFConverter](https://gitlab.com/epicsoft-networks/pdfconverter) library can be used for conversion.

Artifacts: https://gitlab.com/epicsoft-networks/zugferd/-/packages

## Version

Version is made up of the first three digits of the ZUGFeRD version, followed by a fourth digit of the project version.

See [packages](https://gitlab.com/epicsoft-networks/zugferd/-/packages)

## Gradle 

```gradle
maven {
  url 'https://gitlab.com/api/v4/projects/28699387/packages/maven'
}
```

```gradle
implementation 'one.epicsoft:zugferd:<LATEST>'
```


## License

This project is under the same license as the XML Schemas, under "Apache License 2.0". 
For full license see [LICENSE](https://gitlab.com/epicsoft-networks/zugferd/blob/master/LICENCE) file.
