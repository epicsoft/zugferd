package one.epicsoft.zugferd.exceptions;

public class NotImplementedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotImplementedException(final String message) {
        super(message);
    }
}
