package one.epicsoft.zugferd.exceptions;

import jakarta.xml.bind.JAXBException;

public class UncheckedJAXBException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UncheckedJAXBException(final JAXBException e) {
        super(e);
    }
}
