
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HeaderTradeAgreementType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="HeaderTradeAgreementType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="BuyerReference" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="SellerTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType"/>
 *         <element name="BuyerTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType"/>
 *         <element name="SalesAgentTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="BuyerTaxRepresentativeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="SellerTaxRepresentativeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="ProductEndUserTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="ApplicableTradeDeliveryTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeDeliveryTermsType" minOccurs="0"/>
 *         <element name="SellerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="BuyerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="QuotationReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="ContractReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="AdditionalReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="BuyerAgentTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="SpecifiedProcuringProject" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ProcuringProjectType" minOccurs="0"/>
 *         <element name="UltimateCustomerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderTradeAgreementType", propOrder = {
    "buyerReference",
    "sellerTradeParty",
    "buyerTradeParty",
    "salesAgentTradeParty",
    "buyerTaxRepresentativeTradeParty",
    "sellerTaxRepresentativeTradeParty",
    "productEndUserTradeParty",
    "applicableTradeDeliveryTerms",
    "sellerOrderReferencedDocument",
    "buyerOrderReferencedDocument",
    "quotationReferencedDocument",
    "contractReferencedDocument",
    "additionalReferencedDocuments",
    "buyerAgentTradeParty",
    "specifiedProcuringProject",
    "ultimateCustomerOrderReferencedDocuments"
})
public class HeaderTradeAgreementType {

    @XmlElement(name = "BuyerReference")
    protected TextType buyerReference;
    @XmlElement(name = "SellerTradeParty", required = true)
    protected TradePartyType sellerTradeParty;
    @XmlElement(name = "BuyerTradeParty", required = true)
    protected TradePartyType buyerTradeParty;
    @XmlElement(name = "SalesAgentTradeParty")
    protected TradePartyType salesAgentTradeParty;
    @XmlElement(name = "BuyerTaxRepresentativeTradeParty")
    protected TradePartyType buyerTaxRepresentativeTradeParty;
    @XmlElement(name = "SellerTaxRepresentativeTradeParty")
    protected TradePartyType sellerTaxRepresentativeTradeParty;
    @XmlElement(name = "ProductEndUserTradeParty")
    protected TradePartyType productEndUserTradeParty;
    @XmlElement(name = "ApplicableTradeDeliveryTerms")
    protected TradeDeliveryTermsType applicableTradeDeliveryTerms;
    @XmlElement(name = "SellerOrderReferencedDocument")
    protected ReferencedDocumentType sellerOrderReferencedDocument;
    @XmlElement(name = "BuyerOrderReferencedDocument")
    protected ReferencedDocumentType buyerOrderReferencedDocument;
    @XmlElement(name = "QuotationReferencedDocument")
    protected ReferencedDocumentType quotationReferencedDocument;
    @XmlElement(name = "ContractReferencedDocument")
    protected ReferencedDocumentType contractReferencedDocument;
    @XmlElement(name = "AdditionalReferencedDocument")
    protected List<ReferencedDocumentType> additionalReferencedDocuments;
    @XmlElement(name = "BuyerAgentTradeParty")
    protected TradePartyType buyerAgentTradeParty;
    @XmlElement(name = "SpecifiedProcuringProject")
    protected ProcuringProjectType specifiedProcuringProject;
    @XmlElement(name = "UltimateCustomerOrderReferencedDocument")
    protected List<ReferencedDocumentType> ultimateCustomerOrderReferencedDocuments;

    /**
     * Gets the value of the buyerReference property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getBuyerReference() {
        return buyerReference;
    }

    /**
     * Sets the value of the buyerReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setBuyerReference(TextType value) {
        this.buyerReference = value;
    }

    /**
     * Gets the value of the sellerTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getSellerTradeParty() {
        return sellerTradeParty;
    }

    /**
     * Sets the value of the sellerTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setSellerTradeParty(TradePartyType value) {
        this.sellerTradeParty = value;
    }

    /**
     * Gets the value of the buyerTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getBuyerTradeParty() {
        return buyerTradeParty;
    }

    /**
     * Sets the value of the buyerTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setBuyerTradeParty(TradePartyType value) {
        this.buyerTradeParty = value;
    }

    /**
     * Gets the value of the salesAgentTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getSalesAgentTradeParty() {
        return salesAgentTradeParty;
    }

    /**
     * Sets the value of the salesAgentTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setSalesAgentTradeParty(TradePartyType value) {
        this.salesAgentTradeParty = value;
    }

    /**
     * Gets the value of the buyerTaxRepresentativeTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getBuyerTaxRepresentativeTradeParty() {
        return buyerTaxRepresentativeTradeParty;
    }

    /**
     * Sets the value of the buyerTaxRepresentativeTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setBuyerTaxRepresentativeTradeParty(TradePartyType value) {
        this.buyerTaxRepresentativeTradeParty = value;
    }

    /**
     * Gets the value of the sellerTaxRepresentativeTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getSellerTaxRepresentativeTradeParty() {
        return sellerTaxRepresentativeTradeParty;
    }

    /**
     * Sets the value of the sellerTaxRepresentativeTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setSellerTaxRepresentativeTradeParty(TradePartyType value) {
        this.sellerTaxRepresentativeTradeParty = value;
    }

    /**
     * Gets the value of the productEndUserTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getProductEndUserTradeParty() {
        return productEndUserTradeParty;
    }

    /**
     * Sets the value of the productEndUserTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setProductEndUserTradeParty(TradePartyType value) {
        this.productEndUserTradeParty = value;
    }

    /**
     * Gets the value of the applicableTradeDeliveryTerms property.
     * 
     * @return
     *     possible object is
     *     {@link TradeDeliveryTermsType }
     *     
     */
    public TradeDeliveryTermsType getApplicableTradeDeliveryTerms() {
        return applicableTradeDeliveryTerms;
    }

    /**
     * Sets the value of the applicableTradeDeliveryTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeDeliveryTermsType }
     *     
     */
    public void setApplicableTradeDeliveryTerms(TradeDeliveryTermsType value) {
        this.applicableTradeDeliveryTerms = value;
    }

    /**
     * Gets the value of the sellerOrderReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getSellerOrderReferencedDocument() {
        return sellerOrderReferencedDocument;
    }

    /**
     * Sets the value of the sellerOrderReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setSellerOrderReferencedDocument(ReferencedDocumentType value) {
        this.sellerOrderReferencedDocument = value;
    }

    /**
     * Gets the value of the buyerOrderReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getBuyerOrderReferencedDocument() {
        return buyerOrderReferencedDocument;
    }

    /**
     * Sets the value of the buyerOrderReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setBuyerOrderReferencedDocument(ReferencedDocumentType value) {
        this.buyerOrderReferencedDocument = value;
    }

    /**
     * Gets the value of the quotationReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getQuotationReferencedDocument() {
        return quotationReferencedDocument;
    }

    /**
     * Sets the value of the quotationReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setQuotationReferencedDocument(ReferencedDocumentType value) {
        this.quotationReferencedDocument = value;
    }

    /**
     * Gets the value of the contractReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getContractReferencedDocument() {
        return contractReferencedDocument;
    }

    /**
     * Sets the value of the contractReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setContractReferencedDocument(ReferencedDocumentType value) {
        this.contractReferencedDocument = value;
    }

    /**
     * Gets the value of the additionalReferencedDocuments property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalReferencedDocuments property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getAdditionalReferencedDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentType }
     * </p>
     * 
     * 
     * @return
     *     The value of the additionalReferencedDocuments property.
     */
    public List<ReferencedDocumentType> getAdditionalReferencedDocuments() {
        if (additionalReferencedDocuments == null) {
            additionalReferencedDocuments = new ArrayList<>();
        }
        return this.additionalReferencedDocuments;
    }

    /**
     * Gets the value of the buyerAgentTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getBuyerAgentTradeParty() {
        return buyerAgentTradeParty;
    }

    /**
     * Sets the value of the buyerAgentTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setBuyerAgentTradeParty(TradePartyType value) {
        this.buyerAgentTradeParty = value;
    }

    /**
     * Gets the value of the specifiedProcuringProject property.
     * 
     * @return
     *     possible object is
     *     {@link ProcuringProjectType }
     *     
     */
    public ProcuringProjectType getSpecifiedProcuringProject() {
        return specifiedProcuringProject;
    }

    /**
     * Sets the value of the specifiedProcuringProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcuringProjectType }
     *     
     */
    public void setSpecifiedProcuringProject(ProcuringProjectType value) {
        this.specifiedProcuringProject = value;
    }

    /**
     * Gets the value of the ultimateCustomerOrderReferencedDocuments property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ultimateCustomerOrderReferencedDocuments property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getUltimateCustomerOrderReferencedDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentType }
     * </p>
     * 
     * 
     * @return
     *     The value of the ultimateCustomerOrderReferencedDocuments property.
     */
    public List<ReferencedDocumentType> getUltimateCustomerOrderReferencedDocuments() {
        if (ultimateCustomerOrderReferencedDocuments == null) {
            ultimateCustomerOrderReferencedDocuments = new ArrayList<>();
        }
        return this.ultimateCustomerOrderReferencedDocuments;
    }

}
