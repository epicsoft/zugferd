
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the one.epicsoft.zugferd.extended package. 
 * <p>An ObjectFactory allows you to programmatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: one.epicsoft.zugferd.extended
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DateType }
     * 
     * @return
     *     the new instance of {@link DateType }
     */
    public DateType createDateType() {
        return new DateType();
    }

    /**
     * Create an instance of {@link DateTimeType }
     * 
     * @return
     *     the new instance of {@link DateTimeType }
     */
    public DateTimeType createDateTimeType() {
        return new DateTimeType();
    }

    /**
     * Create an instance of {@link FormattedDateTimeType }
     * 
     * @return
     *     the new instance of {@link FormattedDateTimeType }
     */
    public FormattedDateTimeType createFormattedDateTimeType() {
        return new FormattedDateTimeType();
    }

    /**
     * Create an instance of {@link CrossIndustryInvoice }
     * 
     * @return
     *     the new instance of {@link CrossIndustryInvoice }
     */
    public CrossIndustryInvoice createCrossIndustryInvoice() {
        return new CrossIndustryInvoice();
    }

    /**
     * Create an instance of {@link ExchangedDocumentContextType }
     * 
     * @return
     *     the new instance of {@link ExchangedDocumentContextType }
     */
    public ExchangedDocumentContextType createExchangedDocumentContextType() {
        return new ExchangedDocumentContextType();
    }

    /**
     * Create an instance of {@link ExchangedDocumentType }
     * 
     * @return
     *     the new instance of {@link ExchangedDocumentType }
     */
    public ExchangedDocumentType createExchangedDocumentType() {
        return new ExchangedDocumentType();
    }

    /**
     * Create an instance of {@link SupplyChainTradeTransactionType }
     * 
     * @return
     *     the new instance of {@link SupplyChainTradeTransactionType }
     */
    public SupplyChainTradeTransactionType createSupplyChainTradeTransactionType() {
        return new SupplyChainTradeTransactionType();
    }

    /**
     * Create an instance of {@link AccountingAccountTypeCodeType }
     * 
     * @return
     *     the new instance of {@link AccountingAccountTypeCodeType }
     */
    public AccountingAccountTypeCodeType createAccountingAccountTypeCodeType() {
        return new AccountingAccountTypeCodeType();
    }

    /**
     * Create an instance of {@link AllowanceChargeReasonCodeType }
     * 
     * @return
     *     the new instance of {@link AllowanceChargeReasonCodeType }
     */
    public AllowanceChargeReasonCodeType createAllowanceChargeReasonCodeType() {
        return new AllowanceChargeReasonCodeType();
    }

    /**
     * Create an instance of {@link ContactTypeCodeType }
     * 
     * @return
     *     the new instance of {@link ContactTypeCodeType }
     */
    public ContactTypeCodeType createContactTypeCodeType() {
        return new ContactTypeCodeType();
    }

    /**
     * Create an instance of {@link CountryIDType }
     * 
     * @return
     *     the new instance of {@link CountryIDType }
     */
    public CountryIDType createCountryIDType() {
        return new CountryIDType();
    }

    /**
     * Create an instance of {@link CurrencyCodeType }
     * 
     * @return
     *     the new instance of {@link CurrencyCodeType }
     */
    public CurrencyCodeType createCurrencyCodeType() {
        return new CurrencyCodeType();
    }

    /**
     * Create an instance of {@link DeliveryTermsCodeType }
     * 
     * @return
     *     the new instance of {@link DeliveryTermsCodeType }
     */
    public DeliveryTermsCodeType createDeliveryTermsCodeType() {
        return new DeliveryTermsCodeType();
    }

    /**
     * Create an instance of {@link DocumentCodeType }
     * 
     * @return
     *     the new instance of {@link DocumentCodeType }
     */
    public DocumentCodeType createDocumentCodeType() {
        return new DocumentCodeType();
    }

    /**
     * Create an instance of {@link LineStatusCodeType }
     * 
     * @return
     *     the new instance of {@link LineStatusCodeType }
     */
    public LineStatusCodeType createLineStatusCodeType() {
        return new LineStatusCodeType();
    }

    /**
     * Create an instance of {@link PartyRoleCodeType }
     * 
     * @return
     *     the new instance of {@link PartyRoleCodeType }
     */
    public PartyRoleCodeType createPartyRoleCodeType() {
        return new PartyRoleCodeType();
    }

    /**
     * Create an instance of {@link PaymentMeansCodeType }
     * 
     * @return
     *     the new instance of {@link PaymentMeansCodeType }
     */
    public PaymentMeansCodeType createPaymentMeansCodeType() {
        return new PaymentMeansCodeType();
    }

    /**
     * Create an instance of {@link ReferenceCodeType }
     * 
     * @return
     *     the new instance of {@link ReferenceCodeType }
     */
    public ReferenceCodeType createReferenceCodeType() {
        return new ReferenceCodeType();
    }

    /**
     * Create an instance of {@link TaxCategoryCodeType }
     * 
     * @return
     *     the new instance of {@link TaxCategoryCodeType }
     */
    public TaxCategoryCodeType createTaxCategoryCodeType() {
        return new TaxCategoryCodeType();
    }

    /**
     * Create an instance of {@link TaxTypeCodeType }
     * 
     * @return
     *     the new instance of {@link TaxTypeCodeType }
     */
    public TaxTypeCodeType createTaxTypeCodeType() {
        return new TaxTypeCodeType();
    }

    /**
     * Create an instance of {@link TimeReferenceCodeType }
     * 
     * @return
     *     the new instance of {@link TimeReferenceCodeType }
     */
    public TimeReferenceCodeType createTimeReferenceCodeType() {
        return new TimeReferenceCodeType();
    }

    /**
     * Create an instance of {@link TransportModeCodeType }
     * 
     * @return
     *     the new instance of {@link TransportModeCodeType }
     */
    public TransportModeCodeType createTransportModeCodeType() {
        return new TransportModeCodeType();
    }

    /**
     * Create an instance of {@link AmountType }
     * 
     * @return
     *     the new instance of {@link AmountType }
     */
    public AmountType createAmountType() {
        return new AmountType();
    }

    /**
     * Create an instance of {@link BinaryObjectType }
     * 
     * @return
     *     the new instance of {@link BinaryObjectType }
     */
    public BinaryObjectType createBinaryObjectType() {
        return new BinaryObjectType();
    }

    /**
     * Create an instance of {@link CodeType }
     * 
     * @return
     *     the new instance of {@link CodeType }
     */
    public CodeType createCodeType() {
        return new CodeType();
    }

    /**
     * Create an instance of {@link IDType }
     * 
     * @return
     *     the new instance of {@link IDType }
     */
    public IDType createIDType() {
        return new IDType();
    }

    /**
     * Create an instance of {@link IndicatorType }
     * 
     * @return
     *     the new instance of {@link IndicatorType }
     */
    public IndicatorType createIndicatorType() {
        return new IndicatorType();
    }

    /**
     * Create an instance of {@link MeasureType }
     * 
     * @return
     *     the new instance of {@link MeasureType }
     */
    public MeasureType createMeasureType() {
        return new MeasureType();
    }

    /**
     * Create an instance of {@link NumericType }
     * 
     * @return
     *     the new instance of {@link NumericType }
     */
    public NumericType createNumericType() {
        return new NumericType();
    }

    /**
     * Create an instance of {@link PercentType }
     * 
     * @return
     *     the new instance of {@link PercentType }
     */
    public PercentType createPercentType() {
        return new PercentType();
    }

    /**
     * Create an instance of {@link QuantityType }
     * 
     * @return
     *     the new instance of {@link QuantityType }
     */
    public QuantityType createQuantityType() {
        return new QuantityType();
    }

    /**
     * Create an instance of {@link RateType }
     * 
     * @return
     *     the new instance of {@link RateType }
     */
    public RateType createRateType() {
        return new RateType();
    }

    /**
     * Create an instance of {@link TextType }
     * 
     * @return
     *     the new instance of {@link TextType }
     */
    public TextType createTextType() {
        return new TextType();
    }

    /**
     * Create an instance of {@link AdvancePaymentType }
     * 
     * @return
     *     the new instance of {@link AdvancePaymentType }
     */
    public AdvancePaymentType createAdvancePaymentType() {
        return new AdvancePaymentType();
    }

    /**
     * Create an instance of {@link CreditorFinancialAccountType }
     * 
     * @return
     *     the new instance of {@link CreditorFinancialAccountType }
     */
    public CreditorFinancialAccountType createCreditorFinancialAccountType() {
        return new CreditorFinancialAccountType();
    }

    /**
     * Create an instance of {@link CreditorFinancialInstitutionType }
     * 
     * @return
     *     the new instance of {@link CreditorFinancialInstitutionType }
     */
    public CreditorFinancialInstitutionType createCreditorFinancialInstitutionType() {
        return new CreditorFinancialInstitutionType();
    }

    /**
     * Create an instance of {@link DebtorFinancialAccountType }
     * 
     * @return
     *     the new instance of {@link DebtorFinancialAccountType }
     */
    public DebtorFinancialAccountType createDebtorFinancialAccountType() {
        return new DebtorFinancialAccountType();
    }

    /**
     * Create an instance of {@link DocumentContextParameterType }
     * 
     * @return
     *     the new instance of {@link DocumentContextParameterType }
     */
    public DocumentContextParameterType createDocumentContextParameterType() {
        return new DocumentContextParameterType();
    }

    /**
     * Create an instance of {@link DocumentLineDocumentType }
     * 
     * @return
     *     the new instance of {@link DocumentLineDocumentType }
     */
    public DocumentLineDocumentType createDocumentLineDocumentType() {
        return new DocumentLineDocumentType();
    }

    /**
     * Create an instance of {@link HeaderTradeAgreementType }
     * 
     * @return
     *     the new instance of {@link HeaderTradeAgreementType }
     */
    public HeaderTradeAgreementType createHeaderTradeAgreementType() {
        return new HeaderTradeAgreementType();
    }

    /**
     * Create an instance of {@link HeaderTradeDeliveryType }
     * 
     * @return
     *     the new instance of {@link HeaderTradeDeliveryType }
     */
    public HeaderTradeDeliveryType createHeaderTradeDeliveryType() {
        return new HeaderTradeDeliveryType();
    }

    /**
     * Create an instance of {@link HeaderTradeSettlementType }
     * 
     * @return
     *     the new instance of {@link HeaderTradeSettlementType }
     */
    public HeaderTradeSettlementType createHeaderTradeSettlementType() {
        return new HeaderTradeSettlementType();
    }

    /**
     * Create an instance of {@link LegalOrganizationType }
     * 
     * @return
     *     the new instance of {@link LegalOrganizationType }
     */
    public LegalOrganizationType createLegalOrganizationType() {
        return new LegalOrganizationType();
    }

    /**
     * Create an instance of {@link LineTradeAgreementType }
     * 
     * @return
     *     the new instance of {@link LineTradeAgreementType }
     */
    public LineTradeAgreementType createLineTradeAgreementType() {
        return new LineTradeAgreementType();
    }

    /**
     * Create an instance of {@link LineTradeDeliveryType }
     * 
     * @return
     *     the new instance of {@link LineTradeDeliveryType }
     */
    public LineTradeDeliveryType createLineTradeDeliveryType() {
        return new LineTradeDeliveryType();
    }

    /**
     * Create an instance of {@link LineTradeSettlementType }
     * 
     * @return
     *     the new instance of {@link LineTradeSettlementType }
     */
    public LineTradeSettlementType createLineTradeSettlementType() {
        return new LineTradeSettlementType();
    }

    /**
     * Create an instance of {@link LogisticsServiceChargeType }
     * 
     * @return
     *     the new instance of {@link LogisticsServiceChargeType }
     */
    public LogisticsServiceChargeType createLogisticsServiceChargeType() {
        return new LogisticsServiceChargeType();
    }

    /**
     * Create an instance of {@link LogisticsTransportMovementType }
     * 
     * @return
     *     the new instance of {@link LogisticsTransportMovementType }
     */
    public LogisticsTransportMovementType createLogisticsTransportMovementType() {
        return new LogisticsTransportMovementType();
    }

    /**
     * Create an instance of {@link NoteType }
     * 
     * @return
     *     the new instance of {@link NoteType }
     */
    public NoteType createNoteType() {
        return new NoteType();
    }

    /**
     * Create an instance of {@link ProcuringProjectType }
     * 
     * @return
     *     the new instance of {@link ProcuringProjectType }
     */
    public ProcuringProjectType createProcuringProjectType() {
        return new ProcuringProjectType();
    }

    /**
     * Create an instance of {@link ProductCharacteristicType }
     * 
     * @return
     *     the new instance of {@link ProductCharacteristicType }
     */
    public ProductCharacteristicType createProductCharacteristicType() {
        return new ProductCharacteristicType();
    }

    /**
     * Create an instance of {@link ProductClassificationType }
     * 
     * @return
     *     the new instance of {@link ProductClassificationType }
     */
    public ProductClassificationType createProductClassificationType() {
        return new ProductClassificationType();
    }

    /**
     * Create an instance of {@link ReferencedDocumentType }
     * 
     * @return
     *     the new instance of {@link ReferencedDocumentType }
     */
    public ReferencedDocumentType createReferencedDocumentType() {
        return new ReferencedDocumentType();
    }

    /**
     * Create an instance of {@link ReferencedProductType }
     * 
     * @return
     *     the new instance of {@link ReferencedProductType }
     */
    public ReferencedProductType createReferencedProductType() {
        return new ReferencedProductType();
    }

    /**
     * Create an instance of {@link SpecifiedPeriodType }
     * 
     * @return
     *     the new instance of {@link SpecifiedPeriodType }
     */
    public SpecifiedPeriodType createSpecifiedPeriodType() {
        return new SpecifiedPeriodType();
    }

    /**
     * Create an instance of {@link SupplyChainConsignmentType }
     * 
     * @return
     *     the new instance of {@link SupplyChainConsignmentType }
     */
    public SupplyChainConsignmentType createSupplyChainConsignmentType() {
        return new SupplyChainConsignmentType();
    }

    /**
     * Create an instance of {@link SupplyChainEventType }
     * 
     * @return
     *     the new instance of {@link SupplyChainEventType }
     */
    public SupplyChainEventType createSupplyChainEventType() {
        return new SupplyChainEventType();
    }

    /**
     * Create an instance of {@link SupplyChainTradeLineItemType }
     * 
     * @return
     *     the new instance of {@link SupplyChainTradeLineItemType }
     */
    public SupplyChainTradeLineItemType createSupplyChainTradeLineItemType() {
        return new SupplyChainTradeLineItemType();
    }

    /**
     * Create an instance of {@link TaxRegistrationType }
     * 
     * @return
     *     the new instance of {@link TaxRegistrationType }
     */
    public TaxRegistrationType createTaxRegistrationType() {
        return new TaxRegistrationType();
    }

    /**
     * Create an instance of {@link TradeAccountingAccountType }
     * 
     * @return
     *     the new instance of {@link TradeAccountingAccountType }
     */
    public TradeAccountingAccountType createTradeAccountingAccountType() {
        return new TradeAccountingAccountType();
    }

    /**
     * Create an instance of {@link TradeAddressType }
     * 
     * @return
     *     the new instance of {@link TradeAddressType }
     */
    public TradeAddressType createTradeAddressType() {
        return new TradeAddressType();
    }

    /**
     * Create an instance of {@link TradeAllowanceChargeType }
     * 
     * @return
     *     the new instance of {@link TradeAllowanceChargeType }
     */
    public TradeAllowanceChargeType createTradeAllowanceChargeType() {
        return new TradeAllowanceChargeType();
    }

    /**
     * Create an instance of {@link TradeContactType }
     * 
     * @return
     *     the new instance of {@link TradeContactType }
     */
    public TradeContactType createTradeContactType() {
        return new TradeContactType();
    }

    /**
     * Create an instance of {@link TradeCountryType }
     * 
     * @return
     *     the new instance of {@link TradeCountryType }
     */
    public TradeCountryType createTradeCountryType() {
        return new TradeCountryType();
    }

    /**
     * Create an instance of {@link TradeCurrencyExchangeType }
     * 
     * @return
     *     the new instance of {@link TradeCurrencyExchangeType }
     */
    public TradeCurrencyExchangeType createTradeCurrencyExchangeType() {
        return new TradeCurrencyExchangeType();
    }

    /**
     * Create an instance of {@link TradeDeliveryTermsType }
     * 
     * @return
     *     the new instance of {@link TradeDeliveryTermsType }
     */
    public TradeDeliveryTermsType createTradeDeliveryTermsType() {
        return new TradeDeliveryTermsType();
    }

    /**
     * Create an instance of {@link TradePartyType }
     * 
     * @return
     *     the new instance of {@link TradePartyType }
     */
    public TradePartyType createTradePartyType() {
        return new TradePartyType();
    }

    /**
     * Create an instance of {@link TradePaymentDiscountTermsType }
     * 
     * @return
     *     the new instance of {@link TradePaymentDiscountTermsType }
     */
    public TradePaymentDiscountTermsType createTradePaymentDiscountTermsType() {
        return new TradePaymentDiscountTermsType();
    }

    /**
     * Create an instance of {@link TradePaymentPenaltyTermsType }
     * 
     * @return
     *     the new instance of {@link TradePaymentPenaltyTermsType }
     */
    public TradePaymentPenaltyTermsType createTradePaymentPenaltyTermsType() {
        return new TradePaymentPenaltyTermsType();
    }

    /**
     * Create an instance of {@link TradePaymentTermsType }
     * 
     * @return
     *     the new instance of {@link TradePaymentTermsType }
     */
    public TradePaymentTermsType createTradePaymentTermsType() {
        return new TradePaymentTermsType();
    }

    /**
     * Create an instance of {@link TradePriceType }
     * 
     * @return
     *     the new instance of {@link TradePriceType }
     */
    public TradePriceType createTradePriceType() {
        return new TradePriceType();
    }

    /**
     * Create an instance of {@link TradeProductInstanceType }
     * 
     * @return
     *     the new instance of {@link TradeProductInstanceType }
     */
    public TradeProductInstanceType createTradeProductInstanceType() {
        return new TradeProductInstanceType();
    }

    /**
     * Create an instance of {@link TradeProductType }
     * 
     * @return
     *     the new instance of {@link TradeProductType }
     */
    public TradeProductType createTradeProductType() {
        return new TradeProductType();
    }

    /**
     * Create an instance of {@link TradeSettlementFinancialCardType }
     * 
     * @return
     *     the new instance of {@link TradeSettlementFinancialCardType }
     */
    public TradeSettlementFinancialCardType createTradeSettlementFinancialCardType() {
        return new TradeSettlementFinancialCardType();
    }

    /**
     * Create an instance of {@link TradeSettlementHeaderMonetarySummationType }
     * 
     * @return
     *     the new instance of {@link TradeSettlementHeaderMonetarySummationType }
     */
    public TradeSettlementHeaderMonetarySummationType createTradeSettlementHeaderMonetarySummationType() {
        return new TradeSettlementHeaderMonetarySummationType();
    }

    /**
     * Create an instance of {@link TradeSettlementLineMonetarySummationType }
     * 
     * @return
     *     the new instance of {@link TradeSettlementLineMonetarySummationType }
     */
    public TradeSettlementLineMonetarySummationType createTradeSettlementLineMonetarySummationType() {
        return new TradeSettlementLineMonetarySummationType();
    }

    /**
     * Create an instance of {@link TradeSettlementPaymentMeansType }
     * 
     * @return
     *     the new instance of {@link TradeSettlementPaymentMeansType }
     */
    public TradeSettlementPaymentMeansType createTradeSettlementPaymentMeansType() {
        return new TradeSettlementPaymentMeansType();
    }

    /**
     * Create an instance of {@link TradeTaxType }
     * 
     * @return
     *     the new instance of {@link TradeTaxType }
     */
    public TradeTaxType createTradeTaxType() {
        return new TradeTaxType();
    }

    /**
     * Create an instance of {@link UniversalCommunicationType }
     * 
     * @return
     *     the new instance of {@link UniversalCommunicationType }
     */
    public UniversalCommunicationType createUniversalCommunicationType() {
        return new UniversalCommunicationType();
    }

    /**
     * Create an instance of {@link DateType.DateString }
     * 
     * @return
     *     the new instance of {@link DateType.DateString }
     */
    public DateType.DateString createDateTypeDateString() {
        return new DateType.DateString();
    }

    /**
     * Create an instance of {@link DateTimeType.DateTimeString }
     * 
     * @return
     *     the new instance of {@link DateTimeType.DateTimeString }
     */
    public DateTimeType.DateTimeString createDateTimeTypeDateTimeString() {
        return new DateTimeType.DateTimeString();
    }

    /**
     * Create an instance of {@link FormattedDateTimeType.DateTimeString }
     * 
     * @return
     *     the new instance of {@link FormattedDateTimeType.DateTimeString }
     */
    public FormattedDateTimeType.DateTimeString createFormattedDateTimeTypeDateTimeString() {
        return new FormattedDateTimeType.DateTimeString();
    }

}
