
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradeProductType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TradeProductType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="ID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="GlobalID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="SellerAssignedID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="BuyerAssignedID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="Name" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType"/>
 *         <element name="Description" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="ApplicableProductCharacteristic" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ProductCharacteristicType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="DesignatedProductClassification" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ProductClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="IndividualTradeProductInstance" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeProductInstanceType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="OriginTradeCountry" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeCountryType" minOccurs="0"/>
 *         <element name="IncludedReferencedProduct" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedProductType" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeProductType", propOrder = {
    "id",
    "globalID",
    "sellerAssignedID",
    "buyerAssignedID",
    "name",
    "description",
    "applicableProductCharacteristics",
    "designatedProductClassifications",
    "individualTradeProductInstances",
    "originTradeCountry",
    "includedReferencedProducts"
})
public class TradeProductType {

    @XmlElement(name = "ID")
    protected IDType id;
    @XmlElement(name = "GlobalID")
    protected IDType globalID;
    @XmlElement(name = "SellerAssignedID")
    protected IDType sellerAssignedID;
    @XmlElement(name = "BuyerAssignedID")
    protected IDType buyerAssignedID;
    @XmlElement(name = "Name", required = true)
    protected TextType name;
    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "ApplicableProductCharacteristic")
    protected List<ProductCharacteristicType> applicableProductCharacteristics;
    @XmlElement(name = "DesignatedProductClassification")
    protected List<ProductClassificationType> designatedProductClassifications;
    @XmlElement(name = "IndividualTradeProductInstance")
    protected List<TradeProductInstanceType> individualTradeProductInstances;
    @XmlElement(name = "OriginTradeCountry")
    protected TradeCountryType originTradeCountry;
    @XmlElement(name = "IncludedReferencedProduct")
    protected List<ReferencedProductType> includedReferencedProducts;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Gets the value of the globalID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getGlobalID() {
        return globalID;
    }

    /**
     * Sets the value of the globalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setGlobalID(IDType value) {
        this.globalID = value;
    }

    /**
     * Gets the value of the sellerAssignedID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getSellerAssignedID() {
        return sellerAssignedID;
    }

    /**
     * Sets the value of the sellerAssignedID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setSellerAssignedID(IDType value) {
        this.sellerAssignedID = value;
    }

    /**
     * Gets the value of the buyerAssignedID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getBuyerAssignedID() {
        return buyerAssignedID;
    }

    /**
     * Sets the value of the buyerAssignedID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setBuyerAssignedID(IDType value) {
        this.buyerAssignedID = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setName(TextType value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Gets the value of the applicableProductCharacteristics property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableProductCharacteristics property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getApplicableProductCharacteristics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductCharacteristicType }
     * </p>
     * 
     * 
     * @return
     *     The value of the applicableProductCharacteristics property.
     */
    public List<ProductCharacteristicType> getApplicableProductCharacteristics() {
        if (applicableProductCharacteristics == null) {
            applicableProductCharacteristics = new ArrayList<>();
        }
        return this.applicableProductCharacteristics;
    }

    /**
     * Gets the value of the designatedProductClassifications property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the designatedProductClassifications property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getDesignatedProductClassifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductClassificationType }
     * </p>
     * 
     * 
     * @return
     *     The value of the designatedProductClassifications property.
     */
    public List<ProductClassificationType> getDesignatedProductClassifications() {
        if (designatedProductClassifications == null) {
            designatedProductClassifications = new ArrayList<>();
        }
        return this.designatedProductClassifications;
    }

    /**
     * Gets the value of the individualTradeProductInstances property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the individualTradeProductInstances property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getIndividualTradeProductInstances().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeProductInstanceType }
     * </p>
     * 
     * 
     * @return
     *     The value of the individualTradeProductInstances property.
     */
    public List<TradeProductInstanceType> getIndividualTradeProductInstances() {
        if (individualTradeProductInstances == null) {
            individualTradeProductInstances = new ArrayList<>();
        }
        return this.individualTradeProductInstances;
    }

    /**
     * Gets the value of the originTradeCountry property.
     * 
     * @return
     *     possible object is
     *     {@link TradeCountryType }
     *     
     */
    public TradeCountryType getOriginTradeCountry() {
        return originTradeCountry;
    }

    /**
     * Sets the value of the originTradeCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeCountryType }
     *     
     */
    public void setOriginTradeCountry(TradeCountryType value) {
        this.originTradeCountry = value;
    }

    /**
     * Gets the value of the includedReferencedProducts property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedReferencedProducts property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getIncludedReferencedProducts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedProductType }
     * </p>
     * 
     * 
     * @return
     *     The value of the includedReferencedProducts property.
     */
    public List<ReferencedProductType> getIncludedReferencedProducts() {
        if (includedReferencedProducts == null) {
            includedReferencedProducts = new ArrayList<>();
        }
        return this.includedReferencedProducts;
    }

}
