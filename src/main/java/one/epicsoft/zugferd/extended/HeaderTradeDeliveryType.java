
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HeaderTradeDeliveryType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="HeaderTradeDeliveryType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="RelatedSupplyChainConsignment" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SupplyChainConsignmentType" minOccurs="0"/>
 *         <element name="ShipToTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="UltimateShipToTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="ShipFromTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="ActualDeliverySupplyChainEvent" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SupplyChainEventType" minOccurs="0"/>
 *         <element name="DespatchAdviceReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="ReceivingAdviceReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="DeliveryNoteReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderTradeDeliveryType", propOrder = {
    "relatedSupplyChainConsignment",
    "shipToTradeParty",
    "ultimateShipToTradeParty",
    "shipFromTradeParty",
    "actualDeliverySupplyChainEvent",
    "despatchAdviceReferencedDocument",
    "receivingAdviceReferencedDocument",
    "deliveryNoteReferencedDocument"
})
public class HeaderTradeDeliveryType {

    @XmlElement(name = "RelatedSupplyChainConsignment")
    protected SupplyChainConsignmentType relatedSupplyChainConsignment;
    @XmlElement(name = "ShipToTradeParty")
    protected TradePartyType shipToTradeParty;
    @XmlElement(name = "UltimateShipToTradeParty")
    protected TradePartyType ultimateShipToTradeParty;
    @XmlElement(name = "ShipFromTradeParty")
    protected TradePartyType shipFromTradeParty;
    @XmlElement(name = "ActualDeliverySupplyChainEvent")
    protected SupplyChainEventType actualDeliverySupplyChainEvent;
    @XmlElement(name = "DespatchAdviceReferencedDocument")
    protected ReferencedDocumentType despatchAdviceReferencedDocument;
    @XmlElement(name = "ReceivingAdviceReferencedDocument")
    protected ReferencedDocumentType receivingAdviceReferencedDocument;
    @XmlElement(name = "DeliveryNoteReferencedDocument")
    protected ReferencedDocumentType deliveryNoteReferencedDocument;

    /**
     * Gets the value of the relatedSupplyChainConsignment property.
     * 
     * @return
     *     possible object is
     *     {@link SupplyChainConsignmentType }
     *     
     */
    public SupplyChainConsignmentType getRelatedSupplyChainConsignment() {
        return relatedSupplyChainConsignment;
    }

    /**
     * Sets the value of the relatedSupplyChainConsignment property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplyChainConsignmentType }
     *     
     */
    public void setRelatedSupplyChainConsignment(SupplyChainConsignmentType value) {
        this.relatedSupplyChainConsignment = value;
    }

    /**
     * Gets the value of the shipToTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getShipToTradeParty() {
        return shipToTradeParty;
    }

    /**
     * Sets the value of the shipToTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setShipToTradeParty(TradePartyType value) {
        this.shipToTradeParty = value;
    }

    /**
     * Gets the value of the ultimateShipToTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getUltimateShipToTradeParty() {
        return ultimateShipToTradeParty;
    }

    /**
     * Sets the value of the ultimateShipToTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setUltimateShipToTradeParty(TradePartyType value) {
        this.ultimateShipToTradeParty = value;
    }

    /**
     * Gets the value of the shipFromTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getShipFromTradeParty() {
        return shipFromTradeParty;
    }

    /**
     * Sets the value of the shipFromTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setShipFromTradeParty(TradePartyType value) {
        this.shipFromTradeParty = value;
    }

    /**
     * Gets the value of the actualDeliverySupplyChainEvent property.
     * 
     * @return
     *     possible object is
     *     {@link SupplyChainEventType }
     *     
     */
    public SupplyChainEventType getActualDeliverySupplyChainEvent() {
        return actualDeliverySupplyChainEvent;
    }

    /**
     * Sets the value of the actualDeliverySupplyChainEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplyChainEventType }
     *     
     */
    public void setActualDeliverySupplyChainEvent(SupplyChainEventType value) {
        this.actualDeliverySupplyChainEvent = value;
    }

    /**
     * Gets the value of the despatchAdviceReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getDespatchAdviceReferencedDocument() {
        return despatchAdviceReferencedDocument;
    }

    /**
     * Sets the value of the despatchAdviceReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setDespatchAdviceReferencedDocument(ReferencedDocumentType value) {
        this.despatchAdviceReferencedDocument = value;
    }

    /**
     * Gets the value of the receivingAdviceReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getReceivingAdviceReferencedDocument() {
        return receivingAdviceReferencedDocument;
    }

    /**
     * Sets the value of the receivingAdviceReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setReceivingAdviceReferencedDocument(ReferencedDocumentType value) {
        this.receivingAdviceReferencedDocument = value;
    }

    /**
     * Gets the value of the deliveryNoteReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getDeliveryNoteReferencedDocument() {
        return deliveryNoteReferencedDocument;
    }

    /**
     * Sets the value of the deliveryNoteReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setDeliveryNoteReferencedDocument(ReferencedDocumentType value) {
        this.deliveryNoteReferencedDocument = value;
    }

}
