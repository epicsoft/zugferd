
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HeaderTradeSettlementType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="HeaderTradeSettlementType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="CreditorReferenceID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="PaymentReference" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="TaxCurrencyCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}CurrencyCodeType" minOccurs="0"/>
 *         <element name="InvoiceCurrencyCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}CurrencyCodeType"/>
 *         <element name="InvoiceIssuerReference" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="InvoicerTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="InvoiceeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="PayeeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="PayerTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *         <element name="TaxApplicableTradeCurrencyExchange" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeCurrencyExchangeType" minOccurs="0"/>
 *         <element name="SpecifiedTradeSettlementPaymentMeans" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeSettlementPaymentMeansType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="ApplicableTradeTax" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeTaxType" maxOccurs="unbounded"/>
 *         <element name="BillingSpecifiedPeriod" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SpecifiedPeriodType" minOccurs="0"/>
 *         <element name="SpecifiedTradeAllowanceCharge" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAllowanceChargeType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="SpecifiedLogisticsServiceCharge" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LogisticsServiceChargeType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="SpecifiedTradePaymentTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePaymentTermsType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="SpecifiedTradeSettlementHeaderMonetarySummation" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeSettlementHeaderMonetarySummationType"/>
 *         <element name="InvoiceReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="ReceivableSpecifiedTradeAccountingAccount" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAccountingAccountType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="SpecifiedAdvancePayment" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}AdvancePaymentType" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderTradeSettlementType", propOrder = {
    "creditorReferenceID",
    "paymentReference",
    "taxCurrencyCode",
    "invoiceCurrencyCode",
    "invoiceIssuerReference",
    "invoicerTradeParty",
    "invoiceeTradeParty",
    "payeeTradeParty",
    "payerTradeParty",
    "taxApplicableTradeCurrencyExchange",
    "specifiedTradeSettlementPaymentMeans",
    "applicableTradeTaxes",
    "billingSpecifiedPeriod",
    "specifiedTradeAllowanceCharges",
    "specifiedLogisticsServiceCharges",
    "specifiedTradePaymentTerms",
    "specifiedTradeSettlementHeaderMonetarySummation",
    "invoiceReferencedDocument",
    "receivableSpecifiedTradeAccountingAccounts",
    "specifiedAdvancePayments"
})
public class HeaderTradeSettlementType {

    @XmlElement(name = "CreditorReferenceID")
    protected IDType creditorReferenceID;
    @XmlElement(name = "PaymentReference")
    protected TextType paymentReference;
    @XmlElement(name = "TaxCurrencyCode")
    protected CurrencyCodeType taxCurrencyCode;
    @XmlElement(name = "InvoiceCurrencyCode", required = true)
    protected CurrencyCodeType invoiceCurrencyCode;
    @XmlElement(name = "InvoiceIssuerReference")
    protected TextType invoiceIssuerReference;
    @XmlElement(name = "InvoicerTradeParty")
    protected TradePartyType invoicerTradeParty;
    @XmlElement(name = "InvoiceeTradeParty")
    protected TradePartyType invoiceeTradeParty;
    @XmlElement(name = "PayeeTradeParty")
    protected TradePartyType payeeTradeParty;
    @XmlElement(name = "PayerTradeParty")
    protected TradePartyType payerTradeParty;
    @XmlElement(name = "TaxApplicableTradeCurrencyExchange")
    protected TradeCurrencyExchangeType taxApplicableTradeCurrencyExchange;
    @XmlElement(name = "SpecifiedTradeSettlementPaymentMeans")
    protected List<TradeSettlementPaymentMeansType> specifiedTradeSettlementPaymentMeans;
    @XmlElement(name = "ApplicableTradeTax", required = true)
    protected List<TradeTaxType> applicableTradeTaxes;
    @XmlElement(name = "BillingSpecifiedPeriod")
    protected SpecifiedPeriodType billingSpecifiedPeriod;
    @XmlElement(name = "SpecifiedTradeAllowanceCharge")
    protected List<TradeAllowanceChargeType> specifiedTradeAllowanceCharges;
    @XmlElement(name = "SpecifiedLogisticsServiceCharge")
    protected List<LogisticsServiceChargeType> specifiedLogisticsServiceCharges;
    @XmlElement(name = "SpecifiedTradePaymentTerms")
    protected List<TradePaymentTermsType> specifiedTradePaymentTerms;
    @XmlElement(name = "SpecifiedTradeSettlementHeaderMonetarySummation", required = true)
    protected TradeSettlementHeaderMonetarySummationType specifiedTradeSettlementHeaderMonetarySummation;
    @XmlElement(name = "InvoiceReferencedDocument")
    protected ReferencedDocumentType invoiceReferencedDocument;
    @XmlElement(name = "ReceivableSpecifiedTradeAccountingAccount")
    protected List<TradeAccountingAccountType> receivableSpecifiedTradeAccountingAccounts;
    @XmlElement(name = "SpecifiedAdvancePayment")
    protected List<AdvancePaymentType> specifiedAdvancePayments;

    /**
     * Gets the value of the creditorReferenceID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getCreditorReferenceID() {
        return creditorReferenceID;
    }

    /**
     * Sets the value of the creditorReferenceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setCreditorReferenceID(IDType value) {
        this.creditorReferenceID = value;
    }

    /**
     * Gets the value of the paymentReference property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getPaymentReference() {
        return paymentReference;
    }

    /**
     * Sets the value of the paymentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setPaymentReference(TextType value) {
        this.paymentReference = value;
    }

    /**
     * Gets the value of the taxCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getTaxCurrencyCode() {
        return taxCurrencyCode;
    }

    /**
     * Sets the value of the taxCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setTaxCurrencyCode(CurrencyCodeType value) {
        this.taxCurrencyCode = value;
    }

    /**
     * Gets the value of the invoiceCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getInvoiceCurrencyCode() {
        return invoiceCurrencyCode;
    }

    /**
     * Sets the value of the invoiceCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setInvoiceCurrencyCode(CurrencyCodeType value) {
        this.invoiceCurrencyCode = value;
    }

    /**
     * Gets the value of the invoiceIssuerReference property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getInvoiceIssuerReference() {
        return invoiceIssuerReference;
    }

    /**
     * Sets the value of the invoiceIssuerReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setInvoiceIssuerReference(TextType value) {
        this.invoiceIssuerReference = value;
    }

    /**
     * Gets the value of the invoicerTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getInvoicerTradeParty() {
        return invoicerTradeParty;
    }

    /**
     * Sets the value of the invoicerTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setInvoicerTradeParty(TradePartyType value) {
        this.invoicerTradeParty = value;
    }

    /**
     * Gets the value of the invoiceeTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getInvoiceeTradeParty() {
        return invoiceeTradeParty;
    }

    /**
     * Sets the value of the invoiceeTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setInvoiceeTradeParty(TradePartyType value) {
        this.invoiceeTradeParty = value;
    }

    /**
     * Gets the value of the payeeTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getPayeeTradeParty() {
        return payeeTradeParty;
    }

    /**
     * Sets the value of the payeeTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setPayeeTradeParty(TradePartyType value) {
        this.payeeTradeParty = value;
    }

    /**
     * Gets the value of the payerTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getPayerTradeParty() {
        return payerTradeParty;
    }

    /**
     * Sets the value of the payerTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setPayerTradeParty(TradePartyType value) {
        this.payerTradeParty = value;
    }

    /**
     * Gets the value of the taxApplicableTradeCurrencyExchange property.
     * 
     * @return
     *     possible object is
     *     {@link TradeCurrencyExchangeType }
     *     
     */
    public TradeCurrencyExchangeType getTaxApplicableTradeCurrencyExchange() {
        return taxApplicableTradeCurrencyExchange;
    }

    /**
     * Sets the value of the taxApplicableTradeCurrencyExchange property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeCurrencyExchangeType }
     *     
     */
    public void setTaxApplicableTradeCurrencyExchange(TradeCurrencyExchangeType value) {
        this.taxApplicableTradeCurrencyExchange = value;
    }

    /**
     * Gets the value of the specifiedTradeSettlementPaymentMeans property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTradeSettlementPaymentMeans property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getSpecifiedTradeSettlementPaymentMeans().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeSettlementPaymentMeansType }
     * </p>
     * 
     * 
     * @return
     *     The value of the specifiedTradeSettlementPaymentMeans property.
     */
    public List<TradeSettlementPaymentMeansType> getSpecifiedTradeSettlementPaymentMeans() {
        if (specifiedTradeSettlementPaymentMeans == null) {
            specifiedTradeSettlementPaymentMeans = new ArrayList<>();
        }
        return this.specifiedTradeSettlementPaymentMeans;
    }

    /**
     * Gets the value of the applicableTradeTaxes property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableTradeTaxes property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getApplicableTradeTaxes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeTaxType }
     * </p>
     * 
     * 
     * @return
     *     The value of the applicableTradeTaxes property.
     */
    public List<TradeTaxType> getApplicableTradeTaxes() {
        if (applicableTradeTaxes == null) {
            applicableTradeTaxes = new ArrayList<>();
        }
        return this.applicableTradeTaxes;
    }

    /**
     * Gets the value of the billingSpecifiedPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link SpecifiedPeriodType }
     *     
     */
    public SpecifiedPeriodType getBillingSpecifiedPeriod() {
        return billingSpecifiedPeriod;
    }

    /**
     * Sets the value of the billingSpecifiedPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecifiedPeriodType }
     *     
     */
    public void setBillingSpecifiedPeriod(SpecifiedPeriodType value) {
        this.billingSpecifiedPeriod = value;
    }

    /**
     * Gets the value of the specifiedTradeAllowanceCharges property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTradeAllowanceCharges property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getSpecifiedTradeAllowanceCharges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeAllowanceChargeType }
     * </p>
     * 
     * 
     * @return
     *     The value of the specifiedTradeAllowanceCharges property.
     */
    public List<TradeAllowanceChargeType> getSpecifiedTradeAllowanceCharges() {
        if (specifiedTradeAllowanceCharges == null) {
            specifiedTradeAllowanceCharges = new ArrayList<>();
        }
        return this.specifiedTradeAllowanceCharges;
    }

    /**
     * Gets the value of the specifiedLogisticsServiceCharges property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedLogisticsServiceCharges property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getSpecifiedLogisticsServiceCharges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LogisticsServiceChargeType }
     * </p>
     * 
     * 
     * @return
     *     The value of the specifiedLogisticsServiceCharges property.
     */
    public List<LogisticsServiceChargeType> getSpecifiedLogisticsServiceCharges() {
        if (specifiedLogisticsServiceCharges == null) {
            specifiedLogisticsServiceCharges = new ArrayList<>();
        }
        return this.specifiedLogisticsServiceCharges;
    }

    /**
     * Gets the value of the specifiedTradePaymentTerms property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTradePaymentTerms property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getSpecifiedTradePaymentTerms().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradePaymentTermsType }
     * </p>
     * 
     * 
     * @return
     *     The value of the specifiedTradePaymentTerms property.
     */
    public List<TradePaymentTermsType> getSpecifiedTradePaymentTerms() {
        if (specifiedTradePaymentTerms == null) {
            specifiedTradePaymentTerms = new ArrayList<>();
        }
        return this.specifiedTradePaymentTerms;
    }

    /**
     * Gets the value of the specifiedTradeSettlementHeaderMonetarySummation property.
     * 
     * @return
     *     possible object is
     *     {@link TradeSettlementHeaderMonetarySummationType }
     *     
     */
    public TradeSettlementHeaderMonetarySummationType getSpecifiedTradeSettlementHeaderMonetarySummation() {
        return specifiedTradeSettlementHeaderMonetarySummation;
    }

    /**
     * Sets the value of the specifiedTradeSettlementHeaderMonetarySummation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeSettlementHeaderMonetarySummationType }
     *     
     */
    public void setSpecifiedTradeSettlementHeaderMonetarySummation(TradeSettlementHeaderMonetarySummationType value) {
        this.specifiedTradeSettlementHeaderMonetarySummation = value;
    }

    /**
     * Gets the value of the invoiceReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getInvoiceReferencedDocument() {
        return invoiceReferencedDocument;
    }

    /**
     * Sets the value of the invoiceReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setInvoiceReferencedDocument(ReferencedDocumentType value) {
        this.invoiceReferencedDocument = value;
    }

    /**
     * Gets the value of the receivableSpecifiedTradeAccountingAccounts property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receivableSpecifiedTradeAccountingAccounts property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getReceivableSpecifiedTradeAccountingAccounts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeAccountingAccountType }
     * </p>
     * 
     * 
     * @return
     *     The value of the receivableSpecifiedTradeAccountingAccounts property.
     */
    public List<TradeAccountingAccountType> getReceivableSpecifiedTradeAccountingAccounts() {
        if (receivableSpecifiedTradeAccountingAccounts == null) {
            receivableSpecifiedTradeAccountingAccounts = new ArrayList<>();
        }
        return this.receivableSpecifiedTradeAccountingAccounts;
    }

    /**
     * Gets the value of the specifiedAdvancePayments property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedAdvancePayments property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getSpecifiedAdvancePayments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdvancePaymentType }
     * </p>
     * 
     * 
     * @return
     *     The value of the specifiedAdvancePayments property.
     */
    public List<AdvancePaymentType> getSpecifiedAdvancePayments() {
        if (specifiedAdvancePayments == null) {
            specifiedAdvancePayments = new ArrayList<>();
        }
        return this.specifiedAdvancePayments;
    }

}
