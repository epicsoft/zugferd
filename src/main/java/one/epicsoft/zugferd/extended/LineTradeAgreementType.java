
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LineTradeAgreementType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="LineTradeAgreementType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="BuyerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="QuotationReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="ContractReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="AdditionalReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="GrossPriceProductTradePrice" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePriceType" minOccurs="0"/>
 *         <element name="NetPriceProductTradePrice" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePriceType"/>
 *         <element name="UltimateCustomerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineTradeAgreementType", propOrder = {
    "buyerOrderReferencedDocument",
    "quotationReferencedDocument",
    "contractReferencedDocument",
    "additionalReferencedDocuments",
    "grossPriceProductTradePrice",
    "netPriceProductTradePrice",
    "ultimateCustomerOrderReferencedDocuments"
})
public class LineTradeAgreementType {

    @XmlElement(name = "BuyerOrderReferencedDocument")
    protected ReferencedDocumentType buyerOrderReferencedDocument;
    @XmlElement(name = "QuotationReferencedDocument")
    protected ReferencedDocumentType quotationReferencedDocument;
    @XmlElement(name = "ContractReferencedDocument")
    protected ReferencedDocumentType contractReferencedDocument;
    @XmlElement(name = "AdditionalReferencedDocument")
    protected List<ReferencedDocumentType> additionalReferencedDocuments;
    @XmlElement(name = "GrossPriceProductTradePrice")
    protected TradePriceType grossPriceProductTradePrice;
    @XmlElement(name = "NetPriceProductTradePrice", required = true)
    protected TradePriceType netPriceProductTradePrice;
    @XmlElement(name = "UltimateCustomerOrderReferencedDocument")
    protected List<ReferencedDocumentType> ultimateCustomerOrderReferencedDocuments;

    /**
     * Gets the value of the buyerOrderReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getBuyerOrderReferencedDocument() {
        return buyerOrderReferencedDocument;
    }

    /**
     * Sets the value of the buyerOrderReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setBuyerOrderReferencedDocument(ReferencedDocumentType value) {
        this.buyerOrderReferencedDocument = value;
    }

    /**
     * Gets the value of the quotationReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getQuotationReferencedDocument() {
        return quotationReferencedDocument;
    }

    /**
     * Sets the value of the quotationReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setQuotationReferencedDocument(ReferencedDocumentType value) {
        this.quotationReferencedDocument = value;
    }

    /**
     * Gets the value of the contractReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getContractReferencedDocument() {
        return contractReferencedDocument;
    }

    /**
     * Sets the value of the contractReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setContractReferencedDocument(ReferencedDocumentType value) {
        this.contractReferencedDocument = value;
    }

    /**
     * Gets the value of the additionalReferencedDocuments property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalReferencedDocuments property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getAdditionalReferencedDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentType }
     * </p>
     * 
     * 
     * @return
     *     The value of the additionalReferencedDocuments property.
     */
    public List<ReferencedDocumentType> getAdditionalReferencedDocuments() {
        if (additionalReferencedDocuments == null) {
            additionalReferencedDocuments = new ArrayList<>();
        }
        return this.additionalReferencedDocuments;
    }

    /**
     * Gets the value of the grossPriceProductTradePrice property.
     * 
     * @return
     *     possible object is
     *     {@link TradePriceType }
     *     
     */
    public TradePriceType getGrossPriceProductTradePrice() {
        return grossPriceProductTradePrice;
    }

    /**
     * Sets the value of the grossPriceProductTradePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePriceType }
     *     
     */
    public void setGrossPriceProductTradePrice(TradePriceType value) {
        this.grossPriceProductTradePrice = value;
    }

    /**
     * Gets the value of the netPriceProductTradePrice property.
     * 
     * @return
     *     possible object is
     *     {@link TradePriceType }
     *     
     */
    public TradePriceType getNetPriceProductTradePrice() {
        return netPriceProductTradePrice;
    }

    /**
     * Sets the value of the netPriceProductTradePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePriceType }
     *     
     */
    public void setNetPriceProductTradePrice(TradePriceType value) {
        this.netPriceProductTradePrice = value;
    }

    /**
     * Gets the value of the ultimateCustomerOrderReferencedDocuments property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ultimateCustomerOrderReferencedDocuments property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getUltimateCustomerOrderReferencedDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentType }
     * </p>
     * 
     * 
     * @return
     *     The value of the ultimateCustomerOrderReferencedDocuments property.
     */
    public List<ReferencedDocumentType> getUltimateCustomerOrderReferencedDocuments() {
        if (ultimateCustomerOrderReferencedDocuments == null) {
            ultimateCustomerOrderReferencedDocuments = new ArrayList<>();
        }
        return this.ultimateCustomerOrderReferencedDocuments;
    }

}
