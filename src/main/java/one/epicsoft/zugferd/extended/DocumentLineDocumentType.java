
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentLineDocumentType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="DocumentLineDocumentType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="LineID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType"/>
 *         <element name="ParentLineID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="LineStatusCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}LineStatusCodeType" minOccurs="0"/>
 *         <element name="LineStatusReasonCode" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}CodeType" minOccurs="0"/>
 *         <element name="IncludedNote" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}NoteType" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentLineDocumentType", propOrder = {
    "lineID",
    "parentLineID",
    "lineStatusCode",
    "lineStatusReasonCode",
    "includedNotes"
})
public class DocumentLineDocumentType {

    @XmlElement(name = "LineID", required = true)
    protected IDType lineID;
    @XmlElement(name = "ParentLineID")
    protected IDType parentLineID;
    @XmlElement(name = "LineStatusCode")
    protected LineStatusCodeType lineStatusCode;
    @XmlElement(name = "LineStatusReasonCode")
    protected CodeType lineStatusReasonCode;
    @XmlElement(name = "IncludedNote")
    protected List<NoteType> includedNotes;

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setLineID(IDType value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the parentLineID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getParentLineID() {
        return parentLineID;
    }

    /**
     * Sets the value of the parentLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setParentLineID(IDType value) {
        this.parentLineID = value;
    }

    /**
     * Gets the value of the lineStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link LineStatusCodeType }
     *     
     */
    public LineStatusCodeType getLineStatusCode() {
        return lineStatusCode;
    }

    /**
     * Sets the value of the lineStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineStatusCodeType }
     *     
     */
    public void setLineStatusCode(LineStatusCodeType value) {
        this.lineStatusCode = value;
    }

    /**
     * Gets the value of the lineStatusReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getLineStatusReasonCode() {
        return lineStatusReasonCode;
    }

    /**
     * Sets the value of the lineStatusReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setLineStatusReasonCode(CodeType value) {
        this.lineStatusReasonCode = value;
    }

    /**
     * Gets the value of the includedNotes property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedNotes property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getIncludedNotes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteType }
     * </p>
     * 
     * 
     * @return
     *     The value of the includedNotes property.
     */
    public List<NoteType> getIncludedNotes() {
        if (includedNotes == null) {
            includedNotes = new ArrayList<>();
        }
        return this.includedNotes;
    }

}
