
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradeTaxType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TradeTaxType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="CalculatedAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="TypeCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}TaxTypeCodeType" minOccurs="0"/>
 *         <element name="ExemptionReason" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="BasisAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="LineTotalBasisAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="AllowanceChargeBasisAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="CategoryCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}TaxCategoryCodeType"/>
 *         <element name="ExemptionReasonCode" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}CodeType" minOccurs="0"/>
 *         <element name="TaxPointDate" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}DateType" minOccurs="0"/>
 *         <element name="DueDateTypeCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}TimeReferenceCodeType" minOccurs="0"/>
 *         <element name="RateApplicablePercent" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}PercentType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeTaxType", propOrder = {
    "calculatedAmount",
    "typeCode",
    "exemptionReason",
    "basisAmount",
    "lineTotalBasisAmount",
    "allowanceChargeBasisAmount",
    "categoryCode",
    "exemptionReasonCode",
    "taxPointDate",
    "dueDateTypeCode",
    "rateApplicablePercent"
})
public class TradeTaxType {

    @XmlElement(name = "CalculatedAmount")
    protected AmountType calculatedAmount;
    @XmlElement(name = "TypeCode")
    protected TaxTypeCodeType typeCode;
    @XmlElement(name = "ExemptionReason")
    protected TextType exemptionReason;
    @XmlElement(name = "BasisAmount")
    protected AmountType basisAmount;
    @XmlElement(name = "LineTotalBasisAmount")
    protected AmountType lineTotalBasisAmount;
    @XmlElement(name = "AllowanceChargeBasisAmount")
    protected AmountType allowanceChargeBasisAmount;
    @XmlElement(name = "CategoryCode", required = true)
    protected TaxCategoryCodeType categoryCode;
    @XmlElement(name = "ExemptionReasonCode")
    protected CodeType exemptionReasonCode;
    @XmlElement(name = "TaxPointDate")
    protected DateType taxPointDate;
    @XmlElement(name = "DueDateTypeCode")
    protected TimeReferenceCodeType dueDateTypeCode;
    @XmlElement(name = "RateApplicablePercent")
    protected PercentType rateApplicablePercent;

    /**
     * Gets the value of the calculatedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getCalculatedAmount() {
        return calculatedAmount;
    }

    /**
     * Sets the value of the calculatedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setCalculatedAmount(AmountType value) {
        this.calculatedAmount = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link TaxTypeCodeType }
     *     
     */
    public TaxTypeCodeType getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxTypeCodeType }
     *     
     */
    public void setTypeCode(TaxTypeCodeType value) {
        this.typeCode = value;
    }

    /**
     * Gets the value of the exemptionReason property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getExemptionReason() {
        return exemptionReason;
    }

    /**
     * Sets the value of the exemptionReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setExemptionReason(TextType value) {
        this.exemptionReason = value;
    }

    /**
     * Gets the value of the basisAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getBasisAmount() {
        return basisAmount;
    }

    /**
     * Sets the value of the basisAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setBasisAmount(AmountType value) {
        this.basisAmount = value;
    }

    /**
     * Gets the value of the lineTotalBasisAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getLineTotalBasisAmount() {
        return lineTotalBasisAmount;
    }

    /**
     * Sets the value of the lineTotalBasisAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setLineTotalBasisAmount(AmountType value) {
        this.lineTotalBasisAmount = value;
    }

    /**
     * Gets the value of the allowanceChargeBasisAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getAllowanceChargeBasisAmount() {
        return allowanceChargeBasisAmount;
    }

    /**
     * Sets the value of the allowanceChargeBasisAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setAllowanceChargeBasisAmount(AmountType value) {
        this.allowanceChargeBasisAmount = value;
    }

    /**
     * Gets the value of the categoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link TaxCategoryCodeType }
     *     
     */
    public TaxCategoryCodeType getCategoryCode() {
        return categoryCode;
    }

    /**
     * Sets the value of the categoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxCategoryCodeType }
     *     
     */
    public void setCategoryCode(TaxCategoryCodeType value) {
        this.categoryCode = value;
    }

    /**
     * Gets the value of the exemptionReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getExemptionReasonCode() {
        return exemptionReasonCode;
    }

    /**
     * Sets the value of the exemptionReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setExemptionReasonCode(CodeType value) {
        this.exemptionReasonCode = value;
    }

    /**
     * Gets the value of the taxPointDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getTaxPointDate() {
        return taxPointDate;
    }

    /**
     * Sets the value of the taxPointDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setTaxPointDate(DateType value) {
        this.taxPointDate = value;
    }

    /**
     * Gets the value of the dueDateTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link TimeReferenceCodeType }
     *     
     */
    public TimeReferenceCodeType getDueDateTypeCode() {
        return dueDateTypeCode;
    }

    /**
     * Sets the value of the dueDateTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeReferenceCodeType }
     *     
     */
    public void setDueDateTypeCode(TimeReferenceCodeType value) {
        this.dueDateTypeCode = value;
    }

    /**
     * Gets the value of the rateApplicablePercent property.
     * 
     * @return
     *     possible object is
     *     {@link PercentType }
     *     
     */
    public PercentType getRateApplicablePercent() {
        return rateApplicablePercent;
    }

    /**
     * Sets the value of the rateApplicablePercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link PercentType }
     *     
     */
    public void setRateApplicablePercent(PercentType value) {
        this.rateApplicablePercent = value;
    }

}
