
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <p>Java class for TaxTypeCodeContentType</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * <pre>{@code
 * <simpleType name="TaxTypeCodeContentType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="AAA"/>
 *     <enumeration value="AAB"/>
 *     <enumeration value="AAC"/>
 *     <enumeration value="AAD"/>
 *     <enumeration value="AAE"/>
 *     <enumeration value="AAF"/>
 *     <enumeration value="AAG"/>
 *     <enumeration value="AAH"/>
 *     <enumeration value="AAI"/>
 *     <enumeration value="AAJ"/>
 *     <enumeration value="AAK"/>
 *     <enumeration value="AAL"/>
 *     <enumeration value="AAM"/>
 *     <enumeration value="ADD"/>
 *     <enumeration value="BOL"/>
 *     <enumeration value="CAP"/>
 *     <enumeration value="CAR"/>
 *     <enumeration value="COC"/>
 *     <enumeration value="CST"/>
 *     <enumeration value="CUD"/>
 *     <enumeration value="CVD"/>
 *     <enumeration value="ENV"/>
 *     <enumeration value="EXC"/>
 *     <enumeration value="EXP"/>
 *     <enumeration value="FET"/>
 *     <enumeration value="FRE"/>
 *     <enumeration value="GCN"/>
 *     <enumeration value="GST"/>
 *     <enumeration value="ILL"/>
 *     <enumeration value="IMP"/>
 *     <enumeration value="IND"/>
 *     <enumeration value="LAC"/>
 *     <enumeration value="LCN"/>
 *     <enumeration value="LDP"/>
 *     <enumeration value="LOC"/>
 *     <enumeration value="LST"/>
 *     <enumeration value="MCA"/>
 *     <enumeration value="MCD"/>
 *     <enumeration value="OTH"/>
 *     <enumeration value="PDB"/>
 *     <enumeration value="PDC"/>
 *     <enumeration value="PRF"/>
 *     <enumeration value="SCN"/>
 *     <enumeration value="SSS"/>
 *     <enumeration value="STT"/>
 *     <enumeration value="SUP"/>
 *     <enumeration value="SUR"/>
 *     <enumeration value="SWT"/>
 *     <enumeration value="TAC"/>
 *     <enumeration value="TOT"/>
 *     <enumeration value="TOX"/>
 *     <enumeration value="TTA"/>
 *     <enumeration value="VAD"/>
 *     <enumeration value="VAT"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TaxTypeCodeContentType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100")
@XmlEnum
public enum TaxTypeCodeContentType {

    AAA,
    AAB,
    AAC,
    AAD,
    AAE,
    AAF,
    AAG,
    AAH,
    AAI,
    AAJ,
    AAK,
    AAL,
    AAM,
    ADD,
    BOL,
    CAP,
    CAR,
    COC,
    CST,
    CUD,
    CVD,
    ENV,
    EXC,
    EXP,
    FET,
    FRE,
    GCN,
    GST,
    ILL,
    IMP,
    IND,
    LAC,
    LCN,
    LDP,
    LOC,
    LST,
    MCA,
    MCD,
    OTH,
    PDB,
    PDC,
    PRF,
    SCN,
    SSS,
    STT,
    SUP,
    SUR,
    SWT,
    TAC,
    TOT,
    TOX,
    TTA,
    VAD,
    VAT;

    public String value() {
        return name();
    }

    public static TaxTypeCodeContentType fromValue(String v) {
        return valueOf(v);
    }

}
