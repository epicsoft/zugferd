
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradePartyType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TradePartyType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="ID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="GlobalID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="Name" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="RoleCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}PartyRoleCodeType" minOccurs="0"/>
 *         <element name="Description" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="SpecifiedLegalOrganization" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LegalOrganizationType" minOccurs="0"/>
 *         <element name="DefinedTradeContact" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeContactType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="PostalTradeAddress" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAddressType" minOccurs="0"/>
 *         <element name="URIUniversalCommunication" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}UniversalCommunicationType" minOccurs="0"/>
 *         <element name="SpecifiedTaxRegistration" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TaxRegistrationType" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradePartyType", propOrder = {
    "ids",
    "globalIDs",
    "name",
    "roleCode",
    "description",
    "specifiedLegalOrganization",
    "definedTradeContacts",
    "postalTradeAddress",
    "uriUniversalCommunication",
    "specifiedTaxRegistrations"
})
public class TradePartyType {

    @XmlElement(name = "ID")
    protected List<IDType> ids;
    @XmlElement(name = "GlobalID")
    protected List<IDType> globalIDs;
    @XmlElement(name = "Name")
    protected TextType name;
    @XmlElement(name = "RoleCode")
    protected PartyRoleCodeType roleCode;
    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "SpecifiedLegalOrganization")
    protected LegalOrganizationType specifiedLegalOrganization;
    @XmlElement(name = "DefinedTradeContact")
    protected List<TradeContactType> definedTradeContacts;
    @XmlElement(name = "PostalTradeAddress")
    protected TradeAddressType postalTradeAddress;
    @XmlElement(name = "URIUniversalCommunication")
    protected UniversalCommunicationType uriUniversalCommunication;
    @XmlElement(name = "SpecifiedTaxRegistration")
    protected List<TaxRegistrationType> specifiedTaxRegistrations;

    /**
     * Gets the value of the ids property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ids property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getIDS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IDType }
     * </p>
     * 
     * 
     * @return
     *     The value of the ids property.
     */
    public List<IDType> getIDS() {
        if (ids == null) {
            ids = new ArrayList<>();
        }
        return this.ids;
    }

    /**
     * Gets the value of the globalIDs property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the globalIDs property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getGlobalIDs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IDType }
     * </p>
     * 
     * 
     * @return
     *     The value of the globalIDs property.
     */
    public List<IDType> getGlobalIDs() {
        if (globalIDs == null) {
            globalIDs = new ArrayList<>();
        }
        return this.globalIDs;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setName(TextType value) {
        this.name = value;
    }

    /**
     * Gets the value of the roleCode property.
     * 
     * @return
     *     possible object is
     *     {@link PartyRoleCodeType }
     *     
     */
    public PartyRoleCodeType getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the value of the roleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyRoleCodeType }
     *     
     */
    public void setRoleCode(PartyRoleCodeType value) {
        this.roleCode = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Gets the value of the specifiedLegalOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link LegalOrganizationType }
     *     
     */
    public LegalOrganizationType getSpecifiedLegalOrganization() {
        return specifiedLegalOrganization;
    }

    /**
     * Sets the value of the specifiedLegalOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link LegalOrganizationType }
     *     
     */
    public void setSpecifiedLegalOrganization(LegalOrganizationType value) {
        this.specifiedLegalOrganization = value;
    }

    /**
     * Gets the value of the definedTradeContacts property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the definedTradeContacts property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getDefinedTradeContacts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeContactType }
     * </p>
     * 
     * 
     * @return
     *     The value of the definedTradeContacts property.
     */
    public List<TradeContactType> getDefinedTradeContacts() {
        if (definedTradeContacts == null) {
            definedTradeContacts = new ArrayList<>();
        }
        return this.definedTradeContacts;
    }

    /**
     * Gets the value of the postalTradeAddress property.
     * 
     * @return
     *     possible object is
     *     {@link TradeAddressType }
     *     
     */
    public TradeAddressType getPostalTradeAddress() {
        return postalTradeAddress;
    }

    /**
     * Sets the value of the postalTradeAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeAddressType }
     *     
     */
    public void setPostalTradeAddress(TradeAddressType value) {
        this.postalTradeAddress = value;
    }

    /**
     * Gets the value of the uriUniversalCommunication property.
     * 
     * @return
     *     possible object is
     *     {@link UniversalCommunicationType }
     *     
     */
    public UniversalCommunicationType getURIUniversalCommunication() {
        return uriUniversalCommunication;
    }

    /**
     * Sets the value of the uriUniversalCommunication property.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalCommunicationType }
     *     
     */
    public void setURIUniversalCommunication(UniversalCommunicationType value) {
        this.uriUniversalCommunication = value;
    }

    /**
     * Gets the value of the specifiedTaxRegistrations property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTaxRegistrations property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getSpecifiedTaxRegistrations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxRegistrationType }
     * </p>
     * 
     * 
     * @return
     *     The value of the specifiedTaxRegistrations property.
     */
    public List<TaxRegistrationType> getSpecifiedTaxRegistrations() {
        if (specifiedTaxRegistrations == null) {
            specifiedTaxRegistrations = new ArrayList<>();
        }
        return this.specifiedTaxRegistrations;
    }

}
