
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExchangedDocumentContextType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="ExchangedDocumentContextType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="TestIndicator" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IndicatorType" minOccurs="0"/>
 *         <element name="BusinessProcessSpecifiedDocumentContextParameter" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}DocumentContextParameterType" minOccurs="0"/>
 *         <element name="GuidelineSpecifiedDocumentContextParameter" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}DocumentContextParameterType"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangedDocumentContextType", propOrder = {
    "testIndicator",
    "businessProcessSpecifiedDocumentContextParameter",
    "guidelineSpecifiedDocumentContextParameter"
})
public class ExchangedDocumentContextType {

    @XmlElement(name = "TestIndicator")
    protected IndicatorType testIndicator;
    @XmlElement(name = "BusinessProcessSpecifiedDocumentContextParameter")
    protected DocumentContextParameterType businessProcessSpecifiedDocumentContextParameter;
    @XmlElement(name = "GuidelineSpecifiedDocumentContextParameter", required = true)
    protected DocumentContextParameterType guidelineSpecifiedDocumentContextParameter;

    /**
     * Gets the value of the testIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link IndicatorType }
     *     
     */
    public IndicatorType getTestIndicator() {
        return testIndicator;
    }

    /**
     * Sets the value of the testIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndicatorType }
     *     
     */
    public void setTestIndicator(IndicatorType value) {
        this.testIndicator = value;
    }

    /**
     * Gets the value of the businessProcessSpecifiedDocumentContextParameter property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public DocumentContextParameterType getBusinessProcessSpecifiedDocumentContextParameter() {
        return businessProcessSpecifiedDocumentContextParameter;
    }

    /**
     * Sets the value of the businessProcessSpecifiedDocumentContextParameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public void setBusinessProcessSpecifiedDocumentContextParameter(DocumentContextParameterType value) {
        this.businessProcessSpecifiedDocumentContextParameter = value;
    }

    /**
     * Gets the value of the guidelineSpecifiedDocumentContextParameter property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public DocumentContextParameterType getGuidelineSpecifiedDocumentContextParameter() {
        return guidelineSpecifiedDocumentContextParameter;
    }

    /**
     * Sets the value of the guidelineSpecifiedDocumentContextParameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public void setGuidelineSpecifiedDocumentContextParameter(DocumentContextParameterType value) {
        this.guidelineSpecifiedDocumentContextParameter = value;
    }

}
