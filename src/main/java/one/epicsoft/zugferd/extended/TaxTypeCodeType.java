
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for TaxTypeCodeType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TaxTypeCodeType">
 *   <simpleContent>
 *     <extension base="<urn:un:unece:uncefact:data:standard:QualifiedDataType:100>TaxTypeCodeContentType">
 *     </extension>
 *   </simpleContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxTypeCodeType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100", propOrder = {
    "value"
})
public class TaxTypeCodeType {

    @XmlValue
    protected TaxTypeCodeContentType value;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link TaxTypeCodeContentType }
     *     
     */
    public TaxTypeCodeContentType getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxTypeCodeContentType }
     *     
     */
    public void setValue(TaxTypeCodeContentType value) {
        this.value = value;
    }

}
