
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SupplyChainTradeLineItemType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="SupplyChainTradeLineItemType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="AssociatedDocumentLineDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}DocumentLineDocumentType"/>
 *         <element name="SpecifiedTradeProduct" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeProductType"/>
 *         <element name="SpecifiedLineTradeAgreement" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LineTradeAgreementType"/>
 *         <element name="SpecifiedLineTradeDelivery" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LineTradeDeliveryType"/>
 *         <element name="SpecifiedLineTradeSettlement" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LineTradeSettlementType"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupplyChainTradeLineItemType", propOrder = {
    "associatedDocumentLineDocument",
    "specifiedTradeProduct",
    "specifiedLineTradeAgreement",
    "specifiedLineTradeDelivery",
    "specifiedLineTradeSettlement"
})
public class SupplyChainTradeLineItemType {

    @XmlElement(name = "AssociatedDocumentLineDocument", required = true)
    protected DocumentLineDocumentType associatedDocumentLineDocument;
    @XmlElement(name = "SpecifiedTradeProduct", required = true)
    protected TradeProductType specifiedTradeProduct;
    @XmlElement(name = "SpecifiedLineTradeAgreement", required = true)
    protected LineTradeAgreementType specifiedLineTradeAgreement;
    @XmlElement(name = "SpecifiedLineTradeDelivery", required = true)
    protected LineTradeDeliveryType specifiedLineTradeDelivery;
    @XmlElement(name = "SpecifiedLineTradeSettlement", required = true)
    protected LineTradeSettlementType specifiedLineTradeSettlement;

    /**
     * Gets the value of the associatedDocumentLineDocument property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentLineDocumentType }
     *     
     */
    public DocumentLineDocumentType getAssociatedDocumentLineDocument() {
        return associatedDocumentLineDocument;
    }

    /**
     * Sets the value of the associatedDocumentLineDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentLineDocumentType }
     *     
     */
    public void setAssociatedDocumentLineDocument(DocumentLineDocumentType value) {
        this.associatedDocumentLineDocument = value;
    }

    /**
     * Gets the value of the specifiedTradeProduct property.
     * 
     * @return
     *     possible object is
     *     {@link TradeProductType }
     *     
     */
    public TradeProductType getSpecifiedTradeProduct() {
        return specifiedTradeProduct;
    }

    /**
     * Sets the value of the specifiedTradeProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeProductType }
     *     
     */
    public void setSpecifiedTradeProduct(TradeProductType value) {
        this.specifiedTradeProduct = value;
    }

    /**
     * Gets the value of the specifiedLineTradeAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link LineTradeAgreementType }
     *     
     */
    public LineTradeAgreementType getSpecifiedLineTradeAgreement() {
        return specifiedLineTradeAgreement;
    }

    /**
     * Sets the value of the specifiedLineTradeAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineTradeAgreementType }
     *     
     */
    public void setSpecifiedLineTradeAgreement(LineTradeAgreementType value) {
        this.specifiedLineTradeAgreement = value;
    }

    /**
     * Gets the value of the specifiedLineTradeDelivery property.
     * 
     * @return
     *     possible object is
     *     {@link LineTradeDeliveryType }
     *     
     */
    public LineTradeDeliveryType getSpecifiedLineTradeDelivery() {
        return specifiedLineTradeDelivery;
    }

    /**
     * Sets the value of the specifiedLineTradeDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineTradeDeliveryType }
     *     
     */
    public void setSpecifiedLineTradeDelivery(LineTradeDeliveryType value) {
        this.specifiedLineTradeDelivery = value;
    }

    /**
     * Gets the value of the specifiedLineTradeSettlement property.
     * 
     * @return
     *     possible object is
     *     {@link LineTradeSettlementType }
     *     
     */
    public LineTradeSettlementType getSpecifiedLineTradeSettlement() {
        return specifiedLineTradeSettlement;
    }

    /**
     * Sets the value of the specifiedLineTradeSettlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineTradeSettlementType }
     *     
     */
    public void setSpecifiedLineTradeSettlement(LineTradeSettlementType value) {
        this.specifiedLineTradeSettlement = value;
    }

}
