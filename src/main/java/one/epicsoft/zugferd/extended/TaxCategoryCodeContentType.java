
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <p>Java class for TaxCategoryCodeContentType</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * <pre>{@code
 * <simpleType name="TaxCategoryCodeContentType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="A"/>
 *     <enumeration value="AA"/>
 *     <enumeration value="AB"/>
 *     <enumeration value="AC"/>
 *     <enumeration value="AD"/>
 *     <enumeration value="AE"/>
 *     <enumeration value="B"/>
 *     <enumeration value="C"/>
 *     <enumeration value="D"/>
 *     <enumeration value="E"/>
 *     <enumeration value="F"/>
 *     <enumeration value="G"/>
 *     <enumeration value="H"/>
 *     <enumeration value="I"/>
 *     <enumeration value="J"/>
 *     <enumeration value="K"/>
 *     <enumeration value="L"/>
 *     <enumeration value="M"/>
 *     <enumeration value="O"/>
 *     <enumeration value="S"/>
 *     <enumeration value="Z"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TaxCategoryCodeContentType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100")
@XmlEnum
public enum TaxCategoryCodeContentType {

    A,
    AA,
    AB,
    AC,
    AD,
    AE,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    O,
    S,
    Z;

    public String value() {
        return name();
    }

    public static TaxCategoryCodeContentType fromValue(String v) {
        return valueOf(v);
    }

}
