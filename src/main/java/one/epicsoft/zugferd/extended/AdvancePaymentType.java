
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdvancePaymentType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="AdvancePaymentType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="PaidAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType"/>
 *         <element name="FormattedReceivedDateTime" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}FormattedDateTimeType" minOccurs="0"/>
 *         <element name="IncludedTradeTax" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeTaxType" maxOccurs="unbounded"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdvancePaymentType", propOrder = {
    "paidAmount",
    "formattedReceivedDateTime",
    "includedTradeTaxes"
})
public class AdvancePaymentType {

    @XmlElement(name = "PaidAmount", required = true)
    protected AmountType paidAmount;
    @XmlElement(name = "FormattedReceivedDateTime")
    protected FormattedDateTimeType formattedReceivedDateTime;
    @XmlElement(name = "IncludedTradeTax", required = true)
    protected List<TradeTaxType> includedTradeTaxes;

    /**
     * Gets the value of the paidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getPaidAmount() {
        return paidAmount;
    }

    /**
     * Sets the value of the paidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setPaidAmount(AmountType value) {
        this.paidAmount = value;
    }

    /**
     * Gets the value of the formattedReceivedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link FormattedDateTimeType }
     *     
     */
    public FormattedDateTimeType getFormattedReceivedDateTime() {
        return formattedReceivedDateTime;
    }

    /**
     * Sets the value of the formattedReceivedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormattedDateTimeType }
     *     
     */
    public void setFormattedReceivedDateTime(FormattedDateTimeType value) {
        this.formattedReceivedDateTime = value;
    }

    /**
     * Gets the value of the includedTradeTaxes property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedTradeTaxes property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getIncludedTradeTaxes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeTaxType }
     * </p>
     * 
     * 
     * @return
     *     The value of the includedTradeTaxes property.
     */
    public List<TradeTaxType> getIncludedTradeTaxes() {
        if (includedTradeTaxes == null) {
            includedTradeTaxes = new ArrayList<>();
        }
        return this.includedTradeTaxes;
    }

}
