
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for TaxCategoryCodeType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TaxCategoryCodeType">
 *   <simpleContent>
 *     <extension base="<urn:un:unece:uncefact:data:standard:QualifiedDataType:100>TaxCategoryCodeContentType">
 *     </extension>
 *   </simpleContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxCategoryCodeType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100", propOrder = {
    "value"
})
public class TaxCategoryCodeType {

    @XmlValue
    protected TaxCategoryCodeContentType value;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link TaxCategoryCodeContentType }
     *     
     */
    public TaxCategoryCodeContentType getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxCategoryCodeContentType }
     *     
     */
    public void setValue(TaxCategoryCodeContentType value) {
        this.value = value;
    }

}
