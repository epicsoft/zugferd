
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradePaymentTermsType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TradePaymentTermsType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="Description" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/>
 *         <element name="DueDateDateTime" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}DateTimeType" minOccurs="0"/>
 *         <element name="DirectDebitMandateID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="PartialPaymentAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="ApplicableTradePaymentPenaltyTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePaymentPenaltyTermsType" minOccurs="0"/>
 *         <element name="ApplicableTradePaymentDiscountTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePaymentDiscountTermsType" minOccurs="0"/>
 *         <element name="PayeeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradePaymentTermsType", propOrder = {
    "description",
    "dueDateDateTime",
    "directDebitMandateID",
    "partialPaymentAmount",
    "applicableTradePaymentPenaltyTerms",
    "applicableTradePaymentDiscountTerms",
    "payeeTradeParty"
})
public class TradePaymentTermsType {

    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "DueDateDateTime")
    protected DateTimeType dueDateDateTime;
    @XmlElement(name = "DirectDebitMandateID")
    protected IDType directDebitMandateID;
    @XmlElement(name = "PartialPaymentAmount")
    protected AmountType partialPaymentAmount;
    @XmlElement(name = "ApplicableTradePaymentPenaltyTerms")
    protected TradePaymentPenaltyTermsType applicableTradePaymentPenaltyTerms;
    @XmlElement(name = "ApplicableTradePaymentDiscountTerms")
    protected TradePaymentDiscountTermsType applicableTradePaymentDiscountTerms;
    @XmlElement(name = "PayeeTradeParty")
    protected TradePartyType payeeTradeParty;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Gets the value of the dueDateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeType }
     *     
     */
    public DateTimeType getDueDateDateTime() {
        return dueDateDateTime;
    }

    /**
     * Sets the value of the dueDateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeType }
     *     
     */
    public void setDueDateDateTime(DateTimeType value) {
        this.dueDateDateTime = value;
    }

    /**
     * Gets the value of the directDebitMandateID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getDirectDebitMandateID() {
        return directDebitMandateID;
    }

    /**
     * Sets the value of the directDebitMandateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setDirectDebitMandateID(IDType value) {
        this.directDebitMandateID = value;
    }

    /**
     * Gets the value of the partialPaymentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getPartialPaymentAmount() {
        return partialPaymentAmount;
    }

    /**
     * Sets the value of the partialPaymentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setPartialPaymentAmount(AmountType value) {
        this.partialPaymentAmount = value;
    }

    /**
     * Gets the value of the applicableTradePaymentPenaltyTerms property.
     * 
     * @return
     *     possible object is
     *     {@link TradePaymentPenaltyTermsType }
     *     
     */
    public TradePaymentPenaltyTermsType getApplicableTradePaymentPenaltyTerms() {
        return applicableTradePaymentPenaltyTerms;
    }

    /**
     * Sets the value of the applicableTradePaymentPenaltyTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePaymentPenaltyTermsType }
     *     
     */
    public void setApplicableTradePaymentPenaltyTerms(TradePaymentPenaltyTermsType value) {
        this.applicableTradePaymentPenaltyTerms = value;
    }

    /**
     * Gets the value of the applicableTradePaymentDiscountTerms property.
     * 
     * @return
     *     possible object is
     *     {@link TradePaymentDiscountTermsType }
     *     
     */
    public TradePaymentDiscountTermsType getApplicableTradePaymentDiscountTerms() {
        return applicableTradePaymentDiscountTerms;
    }

    /**
     * Sets the value of the applicableTradePaymentDiscountTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePaymentDiscountTermsType }
     *     
     */
    public void setApplicableTradePaymentDiscountTerms(TradePaymentDiscountTermsType value) {
        this.applicableTradePaymentDiscountTerms = value;
    }

    /**
     * Gets the value of the payeeTradeParty property.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getPayeeTradeParty() {
        return payeeTradeParty;
    }

    /**
     * Sets the value of the payeeTradeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setPayeeTradeParty(TradePartyType value) {
        this.payeeTradeParty = value;
    }

}
