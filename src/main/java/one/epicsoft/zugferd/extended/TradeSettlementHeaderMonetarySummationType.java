
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradeSettlementHeaderMonetarySummationType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TradeSettlementHeaderMonetarySummationType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="LineTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType"/>
 *         <element name="ChargeTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="AllowanceTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="TaxBasisTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" maxOccurs="2"/>
 *         <element name="TaxTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" maxOccurs="2" minOccurs="0"/>
 *         <element name="RoundingAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="GrandTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" maxOccurs="2"/>
 *         <element name="TotalPrepaidAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="DuePayableAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeSettlementHeaderMonetarySummationType", propOrder = {
    "lineTotalAmount",
    "chargeTotalAmount",
    "allowanceTotalAmount",
    "taxBasisTotalAmounts",
    "taxTotalAmounts",
    "roundingAmount",
    "grandTotalAmounts",
    "totalPrepaidAmount",
    "duePayableAmount"
})
public class TradeSettlementHeaderMonetarySummationType {

    @XmlElement(name = "LineTotalAmount", required = true)
    protected AmountType lineTotalAmount;
    @XmlElement(name = "ChargeTotalAmount")
    protected AmountType chargeTotalAmount;
    @XmlElement(name = "AllowanceTotalAmount")
    protected AmountType allowanceTotalAmount;
    @XmlElement(name = "TaxBasisTotalAmount", required = true)
    protected List<AmountType> taxBasisTotalAmounts;
    @XmlElement(name = "TaxTotalAmount")
    protected List<AmountType> taxTotalAmounts;
    @XmlElement(name = "RoundingAmount")
    protected AmountType roundingAmount;
    @XmlElement(name = "GrandTotalAmount", required = true)
    protected List<AmountType> grandTotalAmounts;
    @XmlElement(name = "TotalPrepaidAmount")
    protected AmountType totalPrepaidAmount;
    @XmlElement(name = "DuePayableAmount", required = true)
    protected AmountType duePayableAmount;

    /**
     * Gets the value of the lineTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getLineTotalAmount() {
        return lineTotalAmount;
    }

    /**
     * Sets the value of the lineTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setLineTotalAmount(AmountType value) {
        this.lineTotalAmount = value;
    }

    /**
     * Gets the value of the chargeTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getChargeTotalAmount() {
        return chargeTotalAmount;
    }

    /**
     * Sets the value of the chargeTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setChargeTotalAmount(AmountType value) {
        this.chargeTotalAmount = value;
    }

    /**
     * Gets the value of the allowanceTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getAllowanceTotalAmount() {
        return allowanceTotalAmount;
    }

    /**
     * Sets the value of the allowanceTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setAllowanceTotalAmount(AmountType value) {
        this.allowanceTotalAmount = value;
    }

    /**
     * Gets the value of the taxBasisTotalAmounts property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxBasisTotalAmounts property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getTaxBasisTotalAmounts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AmountType }
     * </p>
     * 
     * 
     * @return
     *     The value of the taxBasisTotalAmounts property.
     */
    public List<AmountType> getTaxBasisTotalAmounts() {
        if (taxBasisTotalAmounts == null) {
            taxBasisTotalAmounts = new ArrayList<>();
        }
        return this.taxBasisTotalAmounts;
    }

    /**
     * Gets the value of the taxTotalAmounts property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxTotalAmounts property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getTaxTotalAmounts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AmountType }
     * </p>
     * 
     * 
     * @return
     *     The value of the taxTotalAmounts property.
     */
    public List<AmountType> getTaxTotalAmounts() {
        if (taxTotalAmounts == null) {
            taxTotalAmounts = new ArrayList<>();
        }
        return this.taxTotalAmounts;
    }

    /**
     * Gets the value of the roundingAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getRoundingAmount() {
        return roundingAmount;
    }

    /**
     * Sets the value of the roundingAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setRoundingAmount(AmountType value) {
        this.roundingAmount = value;
    }

    /**
     * Gets the value of the grandTotalAmounts property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the grandTotalAmounts property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getGrandTotalAmounts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AmountType }
     * </p>
     * 
     * 
     * @return
     *     The value of the grandTotalAmounts property.
     */
    public List<AmountType> getGrandTotalAmounts() {
        if (grandTotalAmounts == null) {
            grandTotalAmounts = new ArrayList<>();
        }
        return this.grandTotalAmounts;
    }

    /**
     * Gets the value of the totalPrepaidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getTotalPrepaidAmount() {
        return totalPrepaidAmount;
    }

    /**
     * Sets the value of the totalPrepaidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setTotalPrepaidAmount(AmountType value) {
        this.totalPrepaidAmount = value;
    }

    /**
     * Gets the value of the duePayableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getDuePayableAmount() {
        return duePayableAmount;
    }

    /**
     * Sets the value of the duePayableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setDuePayableAmount(AmountType value) {
        this.duePayableAmount = value;
    }

}
