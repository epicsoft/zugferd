
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 * 
 * <p>Java class for ReferenceCodeContentType</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * <pre>{@code
 * <simpleType name="ReferenceCodeContentType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="AAA"/>
 *     <enumeration value="AAB"/>
 *     <enumeration value="AAC"/>
 *     <enumeration value="AAD"/>
 *     <enumeration value="AAE"/>
 *     <enumeration value="AAF"/>
 *     <enumeration value="AAG"/>
 *     <enumeration value="AAH"/>
 *     <enumeration value="AAI"/>
 *     <enumeration value="AAJ"/>
 *     <enumeration value="AAK"/>
 *     <enumeration value="AAL"/>
 *     <enumeration value="AAM"/>
 *     <enumeration value="AAN"/>
 *     <enumeration value="AAO"/>
 *     <enumeration value="AAP"/>
 *     <enumeration value="AAQ"/>
 *     <enumeration value="AAR"/>
 *     <enumeration value="AAS"/>
 *     <enumeration value="AAT"/>
 *     <enumeration value="AAU"/>
 *     <enumeration value="AAV"/>
 *     <enumeration value="AAW"/>
 *     <enumeration value="AAX"/>
 *     <enumeration value="AAY"/>
 *     <enumeration value="AAZ"/>
 *     <enumeration value="ABA"/>
 *     <enumeration value="ABB"/>
 *     <enumeration value="ABC"/>
 *     <enumeration value="ABD"/>
 *     <enumeration value="ABE"/>
 *     <enumeration value="ABF"/>
 *     <enumeration value="ABG"/>
 *     <enumeration value="ABH"/>
 *     <enumeration value="ABI"/>
 *     <enumeration value="ABJ"/>
 *     <enumeration value="ABK"/>
 *     <enumeration value="ABL"/>
 *     <enumeration value="ABM"/>
 *     <enumeration value="ABN"/>
 *     <enumeration value="ABO"/>
 *     <enumeration value="ABP"/>
 *     <enumeration value="ABQ"/>
 *     <enumeration value="ABR"/>
 *     <enumeration value="ABS"/>
 *     <enumeration value="ABT"/>
 *     <enumeration value="ABU"/>
 *     <enumeration value="ABV"/>
 *     <enumeration value="ABW"/>
 *     <enumeration value="ABX"/>
 *     <enumeration value="ABY"/>
 *     <enumeration value="ABZ"/>
 *     <enumeration value="AC"/>
 *     <enumeration value="ACA"/>
 *     <enumeration value="ACB"/>
 *     <enumeration value="ACC"/>
 *     <enumeration value="ACD"/>
 *     <enumeration value="ACE"/>
 *     <enumeration value="ACF"/>
 *     <enumeration value="ACG"/>
 *     <enumeration value="ACH"/>
 *     <enumeration value="ACI"/>
 *     <enumeration value="ACJ"/>
 *     <enumeration value="ACK"/>
 *     <enumeration value="ACL"/>
 *     <enumeration value="ACN"/>
 *     <enumeration value="ACO"/>
 *     <enumeration value="ACP"/>
 *     <enumeration value="ACQ"/>
 *     <enumeration value="ACR"/>
 *     <enumeration value="ACT"/>
 *     <enumeration value="ACU"/>
 *     <enumeration value="ACV"/>
 *     <enumeration value="ACW"/>
 *     <enumeration value="ACX"/>
 *     <enumeration value="ACY"/>
 *     <enumeration value="ACZ"/>
 *     <enumeration value="ADA"/>
 *     <enumeration value="ADB"/>
 *     <enumeration value="ADC"/>
 *     <enumeration value="ADD"/>
 *     <enumeration value="ADE"/>
 *     <enumeration value="ADF"/>
 *     <enumeration value="ADG"/>
 *     <enumeration value="ADI"/>
 *     <enumeration value="ADJ"/>
 *     <enumeration value="ADK"/>
 *     <enumeration value="ADL"/>
 *     <enumeration value="ADM"/>
 *     <enumeration value="ADN"/>
 *     <enumeration value="ADO"/>
 *     <enumeration value="ADP"/>
 *     <enumeration value="ADQ"/>
 *     <enumeration value="ADT"/>
 *     <enumeration value="ADU"/>
 *     <enumeration value="ADV"/>
 *     <enumeration value="ADW"/>
 *     <enumeration value="ADX"/>
 *     <enumeration value="ADY"/>
 *     <enumeration value="ADZ"/>
 *     <enumeration value="AE"/>
 *     <enumeration value="AEA"/>
 *     <enumeration value="AEB"/>
 *     <enumeration value="AEC"/>
 *     <enumeration value="AED"/>
 *     <enumeration value="AEE"/>
 *     <enumeration value="AEF"/>
 *     <enumeration value="AEG"/>
 *     <enumeration value="AEH"/>
 *     <enumeration value="AEI"/>
 *     <enumeration value="AEJ"/>
 *     <enumeration value="AEK"/>
 *     <enumeration value="AEL"/>
 *     <enumeration value="AEM"/>
 *     <enumeration value="AEN"/>
 *     <enumeration value="AEO"/>
 *     <enumeration value="AEP"/>
 *     <enumeration value="AEQ"/>
 *     <enumeration value="AER"/>
 *     <enumeration value="AES"/>
 *     <enumeration value="AET"/>
 *     <enumeration value="AEU"/>
 *     <enumeration value="AEV"/>
 *     <enumeration value="AEW"/>
 *     <enumeration value="AEX"/>
 *     <enumeration value="AEY"/>
 *     <enumeration value="AEZ"/>
 *     <enumeration value="AF"/>
 *     <enumeration value="AFA"/>
 *     <enumeration value="AFB"/>
 *     <enumeration value="AFC"/>
 *     <enumeration value="AFD"/>
 *     <enumeration value="AFE"/>
 *     <enumeration value="AFF"/>
 *     <enumeration value="AFG"/>
 *     <enumeration value="AFH"/>
 *     <enumeration value="AFI"/>
 *     <enumeration value="AFJ"/>
 *     <enumeration value="AFK"/>
 *     <enumeration value="AFL"/>
 *     <enumeration value="AFM"/>
 *     <enumeration value="AFN"/>
 *     <enumeration value="AFO"/>
 *     <enumeration value="AFP"/>
 *     <enumeration value="AFQ"/>
 *     <enumeration value="AFR"/>
 *     <enumeration value="AFS"/>
 *     <enumeration value="AFT"/>
 *     <enumeration value="AFU"/>
 *     <enumeration value="AFV"/>
 *     <enumeration value="AFW"/>
 *     <enumeration value="AFX"/>
 *     <enumeration value="AFY"/>
 *     <enumeration value="AFZ"/>
 *     <enumeration value="AGA"/>
 *     <enumeration value="AGB"/>
 *     <enumeration value="AGC"/>
 *     <enumeration value="AGD"/>
 *     <enumeration value="AGE"/>
 *     <enumeration value="AGF"/>
 *     <enumeration value="AGG"/>
 *     <enumeration value="AGH"/>
 *     <enumeration value="AGI"/>
 *     <enumeration value="AGJ"/>
 *     <enumeration value="AGK"/>
 *     <enumeration value="AGL"/>
 *     <enumeration value="AGM"/>
 *     <enumeration value="AGN"/>
 *     <enumeration value="AGO"/>
 *     <enumeration value="AGP"/>
 *     <enumeration value="AGQ"/>
 *     <enumeration value="AGR"/>
 *     <enumeration value="AGS"/>
 *     <enumeration value="AGT"/>
 *     <enumeration value="AGU"/>
 *     <enumeration value="AGV"/>
 *     <enumeration value="AGW"/>
 *     <enumeration value="AGX"/>
 *     <enumeration value="AGY"/>
 *     <enumeration value="AGZ"/>
 *     <enumeration value="AHA"/>
 *     <enumeration value="AHB"/>
 *     <enumeration value="AHC"/>
 *     <enumeration value="AHD"/>
 *     <enumeration value="AHE"/>
 *     <enumeration value="AHF"/>
 *     <enumeration value="AHG"/>
 *     <enumeration value="AHH"/>
 *     <enumeration value="AHI"/>
 *     <enumeration value="AHJ"/>
 *     <enumeration value="AHK"/>
 *     <enumeration value="AHL"/>
 *     <enumeration value="AHM"/>
 *     <enumeration value="AHN"/>
 *     <enumeration value="AHO"/>
 *     <enumeration value="AHP"/>
 *     <enumeration value="AHQ"/>
 *     <enumeration value="AHR"/>
 *     <enumeration value="AHS"/>
 *     <enumeration value="AHT"/>
 *     <enumeration value="AHU"/>
 *     <enumeration value="AHV"/>
 *     <enumeration value="AHX"/>
 *     <enumeration value="AHY"/>
 *     <enumeration value="AHZ"/>
 *     <enumeration value="AIA"/>
 *     <enumeration value="AIB"/>
 *     <enumeration value="AIC"/>
 *     <enumeration value="AID"/>
 *     <enumeration value="AIE"/>
 *     <enumeration value="AIF"/>
 *     <enumeration value="AIG"/>
 *     <enumeration value="AIH"/>
 *     <enumeration value="AII"/>
 *     <enumeration value="AIJ"/>
 *     <enumeration value="AIK"/>
 *     <enumeration value="AIL"/>
 *     <enumeration value="AIM"/>
 *     <enumeration value="AIN"/>
 *     <enumeration value="AIO"/>
 *     <enumeration value="AIP"/>
 *     <enumeration value="AIQ"/>
 *     <enumeration value="AIR"/>
 *     <enumeration value="AIS"/>
 *     <enumeration value="AIT"/>
 *     <enumeration value="AIU"/>
 *     <enumeration value="AIV"/>
 *     <enumeration value="AIW"/>
 *     <enumeration value="AIX"/>
 *     <enumeration value="AIY"/>
 *     <enumeration value="AIZ"/>
 *     <enumeration value="AJA"/>
 *     <enumeration value="AJB"/>
 *     <enumeration value="AJC"/>
 *     <enumeration value="AJD"/>
 *     <enumeration value="AJE"/>
 *     <enumeration value="AJF"/>
 *     <enumeration value="AJG"/>
 *     <enumeration value="AJH"/>
 *     <enumeration value="AJI"/>
 *     <enumeration value="AJJ"/>
 *     <enumeration value="AJK"/>
 *     <enumeration value="AJL"/>
 *     <enumeration value="AJM"/>
 *     <enumeration value="AJN"/>
 *     <enumeration value="AJO"/>
 *     <enumeration value="AJP"/>
 *     <enumeration value="AJQ"/>
 *     <enumeration value="AJR"/>
 *     <enumeration value="AJS"/>
 *     <enumeration value="AJT"/>
 *     <enumeration value="AJU"/>
 *     <enumeration value="AJV"/>
 *     <enumeration value="AJW"/>
 *     <enumeration value="AJX"/>
 *     <enumeration value="AJY"/>
 *     <enumeration value="AJZ"/>
 *     <enumeration value="AKA"/>
 *     <enumeration value="AKB"/>
 *     <enumeration value="AKC"/>
 *     <enumeration value="AKD"/>
 *     <enumeration value="AKE"/>
 *     <enumeration value="AKF"/>
 *     <enumeration value="AKG"/>
 *     <enumeration value="AKH"/>
 *     <enumeration value="AKI"/>
 *     <enumeration value="AKJ"/>
 *     <enumeration value="AKK"/>
 *     <enumeration value="AKL"/>
 *     <enumeration value="AKM"/>
 *     <enumeration value="AKN"/>
 *     <enumeration value="AKO"/>
 *     <enumeration value="AKP"/>
 *     <enumeration value="AKQ"/>
 *     <enumeration value="AKR"/>
 *     <enumeration value="AKS"/>
 *     <enumeration value="AKT"/>
 *     <enumeration value="AKU"/>
 *     <enumeration value="AKV"/>
 *     <enumeration value="AKW"/>
 *     <enumeration value="AKX"/>
 *     <enumeration value="AKY"/>
 *     <enumeration value="AKZ"/>
 *     <enumeration value="ALA"/>
 *     <enumeration value="ALB"/>
 *     <enumeration value="ALC"/>
 *     <enumeration value="ALD"/>
 *     <enumeration value="ALE"/>
 *     <enumeration value="ALF"/>
 *     <enumeration value="ALG"/>
 *     <enumeration value="ALH"/>
 *     <enumeration value="ALI"/>
 *     <enumeration value="ALJ"/>
 *     <enumeration value="ALK"/>
 *     <enumeration value="ALL"/>
 *     <enumeration value="ALM"/>
 *     <enumeration value="ALN"/>
 *     <enumeration value="ALO"/>
 *     <enumeration value="ALP"/>
 *     <enumeration value="ALQ"/>
 *     <enumeration value="ALR"/>
 *     <enumeration value="ALS"/>
 *     <enumeration value="ALT"/>
 *     <enumeration value="ALU"/>
 *     <enumeration value="ALV"/>
 *     <enumeration value="ALW"/>
 *     <enumeration value="ALX"/>
 *     <enumeration value="ALY"/>
 *     <enumeration value="ALZ"/>
 *     <enumeration value="AMA"/>
 *     <enumeration value="AMB"/>
 *     <enumeration value="AMC"/>
 *     <enumeration value="AMD"/>
 *     <enumeration value="AME"/>
 *     <enumeration value="AMF"/>
 *     <enumeration value="AMG"/>
 *     <enumeration value="AMH"/>
 *     <enumeration value="AMI"/>
 *     <enumeration value="AMJ"/>
 *     <enumeration value="AMK"/>
 *     <enumeration value="AML"/>
 *     <enumeration value="AMM"/>
 *     <enumeration value="AMN"/>
 *     <enumeration value="AMO"/>
 *     <enumeration value="AMP"/>
 *     <enumeration value="AMQ"/>
 *     <enumeration value="AMR"/>
 *     <enumeration value="AMS"/>
 *     <enumeration value="AMT"/>
 *     <enumeration value="AMU"/>
 *     <enumeration value="AMV"/>
 *     <enumeration value="AMW"/>
 *     <enumeration value="AMX"/>
 *     <enumeration value="AMY"/>
 *     <enumeration value="AMZ"/>
 *     <enumeration value="ANA"/>
 *     <enumeration value="ANB"/>
 *     <enumeration value="ANC"/>
 *     <enumeration value="AND"/>
 *     <enumeration value="ANE"/>
 *     <enumeration value="ANF"/>
 *     <enumeration value="ANG"/>
 *     <enumeration value="ANH"/>
 *     <enumeration value="ANI"/>
 *     <enumeration value="ANJ"/>
 *     <enumeration value="ANK"/>
 *     <enumeration value="ANL"/>
 *     <enumeration value="ANM"/>
 *     <enumeration value="ANN"/>
 *     <enumeration value="ANO"/>
 *     <enumeration value="ANP"/>
 *     <enumeration value="ANQ"/>
 *     <enumeration value="ANR"/>
 *     <enumeration value="ANS"/>
 *     <enumeration value="ANT"/>
 *     <enumeration value="ANU"/>
 *     <enumeration value="ANV"/>
 *     <enumeration value="ANW"/>
 *     <enumeration value="ANX"/>
 *     <enumeration value="ANY"/>
 *     <enumeration value="AOA"/>
 *     <enumeration value="AOD"/>
 *     <enumeration value="AOE"/>
 *     <enumeration value="AOF"/>
 *     <enumeration value="AOG"/>
 *     <enumeration value="AOH"/>
 *     <enumeration value="AOI"/>
 *     <enumeration value="AOJ"/>
 *     <enumeration value="AOK"/>
 *     <enumeration value="AOL"/>
 *     <enumeration value="AOM"/>
 *     <enumeration value="AON"/>
 *     <enumeration value="AOO"/>
 *     <enumeration value="AOP"/>
 *     <enumeration value="AOQ"/>
 *     <enumeration value="AOR"/>
 *     <enumeration value="AOS"/>
 *     <enumeration value="AOT"/>
 *     <enumeration value="AOU"/>
 *     <enumeration value="AOV"/>
 *     <enumeration value="AOW"/>
 *     <enumeration value="AOX"/>
 *     <enumeration value="AOY"/>
 *     <enumeration value="AOZ"/>
 *     <enumeration value="AP"/>
 *     <enumeration value="APA"/>
 *     <enumeration value="APB"/>
 *     <enumeration value="APC"/>
 *     <enumeration value="APD"/>
 *     <enumeration value="APE"/>
 *     <enumeration value="APF"/>
 *     <enumeration value="APG"/>
 *     <enumeration value="APH"/>
 *     <enumeration value="API"/>
 *     <enumeration value="APJ"/>
 *     <enumeration value="APK"/>
 *     <enumeration value="APL"/>
 *     <enumeration value="APM"/>
 *     <enumeration value="APN"/>
 *     <enumeration value="APO"/>
 *     <enumeration value="APP"/>
 *     <enumeration value="APQ"/>
 *     <enumeration value="APR"/>
 *     <enumeration value="APS"/>
 *     <enumeration value="APT"/>
 *     <enumeration value="APU"/>
 *     <enumeration value="APV"/>
 *     <enumeration value="APW"/>
 *     <enumeration value="APX"/>
 *     <enumeration value="APY"/>
 *     <enumeration value="APZ"/>
 *     <enumeration value="AQA"/>
 *     <enumeration value="AQB"/>
 *     <enumeration value="AQC"/>
 *     <enumeration value="AQD"/>
 *     <enumeration value="AQE"/>
 *     <enumeration value="AQF"/>
 *     <enumeration value="AQG"/>
 *     <enumeration value="AQH"/>
 *     <enumeration value="AQI"/>
 *     <enumeration value="AQJ"/>
 *     <enumeration value="AQK"/>
 *     <enumeration value="AQL"/>
 *     <enumeration value="AQM"/>
 *     <enumeration value="AQN"/>
 *     <enumeration value="AQO"/>
 *     <enumeration value="AQP"/>
 *     <enumeration value="AQQ"/>
 *     <enumeration value="AQR"/>
 *     <enumeration value="AQS"/>
 *     <enumeration value="AQT"/>
 *     <enumeration value="AQU"/>
 *     <enumeration value="AQV"/>
 *     <enumeration value="AQW"/>
 *     <enumeration value="AQX"/>
 *     <enumeration value="AQY"/>
 *     <enumeration value="AQZ"/>
 *     <enumeration value="ARA"/>
 *     <enumeration value="ARB"/>
 *     <enumeration value="ARC"/>
 *     <enumeration value="ARD"/>
 *     <enumeration value="ARE"/>
 *     <enumeration value="ARF"/>
 *     <enumeration value="ARG"/>
 *     <enumeration value="ARH"/>
 *     <enumeration value="ARI"/>
 *     <enumeration value="ARJ"/>
 *     <enumeration value="ARK"/>
 *     <enumeration value="ARL"/>
 *     <enumeration value="ARM"/>
 *     <enumeration value="ARN"/>
 *     <enumeration value="ARO"/>
 *     <enumeration value="ARP"/>
 *     <enumeration value="ARQ"/>
 *     <enumeration value="ARR"/>
 *     <enumeration value="ARS"/>
 *     <enumeration value="ART"/>
 *     <enumeration value="ARU"/>
 *     <enumeration value="ARV"/>
 *     <enumeration value="ARW"/>
 *     <enumeration value="ARX"/>
 *     <enumeration value="ARY"/>
 *     <enumeration value="ARZ"/>
 *     <enumeration value="ASA"/>
 *     <enumeration value="ASB"/>
 *     <enumeration value="ASC"/>
 *     <enumeration value="ASD"/>
 *     <enumeration value="ASE"/>
 *     <enumeration value="ASF"/>
 *     <enumeration value="ASG"/>
 *     <enumeration value="ASH"/>
 *     <enumeration value="ASI"/>
 *     <enumeration value="ASJ"/>
 *     <enumeration value="ASK"/>
 *     <enumeration value="ASL"/>
 *     <enumeration value="ASM"/>
 *     <enumeration value="ASN"/>
 *     <enumeration value="ASO"/>
 *     <enumeration value="ASP"/>
 *     <enumeration value="ASQ"/>
 *     <enumeration value="ASR"/>
 *     <enumeration value="ASS"/>
 *     <enumeration value="AST"/>
 *     <enumeration value="ASU"/>
 *     <enumeration value="ASV"/>
 *     <enumeration value="ASW"/>
 *     <enumeration value="ASX"/>
 *     <enumeration value="ASY"/>
 *     <enumeration value="ASZ"/>
 *     <enumeration value="ATA"/>
 *     <enumeration value="ATB"/>
 *     <enumeration value="ATC"/>
 *     <enumeration value="ATD"/>
 *     <enumeration value="ATE"/>
 *     <enumeration value="ATF"/>
 *     <enumeration value="ATG"/>
 *     <enumeration value="ATH"/>
 *     <enumeration value="ATI"/>
 *     <enumeration value="ATJ"/>
 *     <enumeration value="ATK"/>
 *     <enumeration value="ATL"/>
 *     <enumeration value="ATM"/>
 *     <enumeration value="ATN"/>
 *     <enumeration value="ATO"/>
 *     <enumeration value="ATP"/>
 *     <enumeration value="ATQ"/>
 *     <enumeration value="ATR"/>
 *     <enumeration value="ATS"/>
 *     <enumeration value="ATT"/>
 *     <enumeration value="ATU"/>
 *     <enumeration value="ATV"/>
 *     <enumeration value="ATW"/>
 *     <enumeration value="ATX"/>
 *     <enumeration value="ATY"/>
 *     <enumeration value="ATZ"/>
 *     <enumeration value="AU"/>
 *     <enumeration value="AUA"/>
 *     <enumeration value="AUB"/>
 *     <enumeration value="AUC"/>
 *     <enumeration value="AUD"/>
 *     <enumeration value="AUE"/>
 *     <enumeration value="AUF"/>
 *     <enumeration value="AUG"/>
 *     <enumeration value="AUH"/>
 *     <enumeration value="AUI"/>
 *     <enumeration value="AUJ"/>
 *     <enumeration value="AUK"/>
 *     <enumeration value="AUL"/>
 *     <enumeration value="AUM"/>
 *     <enumeration value="AUN"/>
 *     <enumeration value="AUO"/>
 *     <enumeration value="AUP"/>
 *     <enumeration value="AUQ"/>
 *     <enumeration value="AUR"/>
 *     <enumeration value="AUS"/>
 *     <enumeration value="AUT"/>
 *     <enumeration value="AUU"/>
 *     <enumeration value="AUV"/>
 *     <enumeration value="AUW"/>
 *     <enumeration value="AUX"/>
 *     <enumeration value="AUY"/>
 *     <enumeration value="AUZ"/>
 *     <enumeration value="AV"/>
 *     <enumeration value="AVA"/>
 *     <enumeration value="AVB"/>
 *     <enumeration value="AVC"/>
 *     <enumeration value="AVD"/>
 *     <enumeration value="AVE"/>
 *     <enumeration value="AVF"/>
 *     <enumeration value="AVG"/>
 *     <enumeration value="AVH"/>
 *     <enumeration value="AVI"/>
 *     <enumeration value="AVJ"/>
 *     <enumeration value="AVK"/>
 *     <enumeration value="AVL"/>
 *     <enumeration value="AVM"/>
 *     <enumeration value="AVN"/>
 *     <enumeration value="AVO"/>
 *     <enumeration value="AVP"/>
 *     <enumeration value="AVQ"/>
 *     <enumeration value="AVR"/>
 *     <enumeration value="AVS"/>
 *     <enumeration value="AVT"/>
 *     <enumeration value="AVU"/>
 *     <enumeration value="AVV"/>
 *     <enumeration value="AVW"/>
 *     <enumeration value="AVX"/>
 *     <enumeration value="AVY"/>
 *     <enumeration value="AVZ"/>
 *     <enumeration value="AWA"/>
 *     <enumeration value="AWB"/>
 *     <enumeration value="AWC"/>
 *     <enumeration value="AWD"/>
 *     <enumeration value="AWE"/>
 *     <enumeration value="AWF"/>
 *     <enumeration value="AWG"/>
 *     <enumeration value="AWH"/>
 *     <enumeration value="AWI"/>
 *     <enumeration value="AWJ"/>
 *     <enumeration value="AWK"/>
 *     <enumeration value="AWL"/>
 *     <enumeration value="AWM"/>
 *     <enumeration value="AWN"/>
 *     <enumeration value="AWO"/>
 *     <enumeration value="AWP"/>
 *     <enumeration value="AWQ"/>
 *     <enumeration value="AWR"/>
 *     <enumeration value="AWS"/>
 *     <enumeration value="AWT"/>
 *     <enumeration value="AWU"/>
 *     <enumeration value="AWV"/>
 *     <enumeration value="AWW"/>
 *     <enumeration value="AWX"/>
 *     <enumeration value="AWY"/>
 *     <enumeration value="AWZ"/>
 *     <enumeration value="AXA"/>
 *     <enumeration value="AXB"/>
 *     <enumeration value="AXC"/>
 *     <enumeration value="AXD"/>
 *     <enumeration value="AXE"/>
 *     <enumeration value="AXF"/>
 *     <enumeration value="AXG"/>
 *     <enumeration value="AXH"/>
 *     <enumeration value="AXI"/>
 *     <enumeration value="AXJ"/>
 *     <enumeration value="AXK"/>
 *     <enumeration value="AXL"/>
 *     <enumeration value="AXM"/>
 *     <enumeration value="AXN"/>
 *     <enumeration value="AXO"/>
 *     <enumeration value="AXP"/>
 *     <enumeration value="AXQ"/>
 *     <enumeration value="AXR"/>
 *     <enumeration value="AXS"/>
 *     <enumeration value="BA"/>
 *     <enumeration value="BC"/>
 *     <enumeration value="BD"/>
 *     <enumeration value="BE"/>
 *     <enumeration value="BH"/>
 *     <enumeration value="BM"/>
 *     <enumeration value="BN"/>
 *     <enumeration value="BO"/>
 *     <enumeration value="BR"/>
 *     <enumeration value="BT"/>
 *     <enumeration value="BTP"/>
 *     <enumeration value="BW"/>
 *     <enumeration value="CAS"/>
 *     <enumeration value="CAT"/>
 *     <enumeration value="CAU"/>
 *     <enumeration value="CAV"/>
 *     <enumeration value="CAW"/>
 *     <enumeration value="CAX"/>
 *     <enumeration value="CAY"/>
 *     <enumeration value="CAZ"/>
 *     <enumeration value="CBA"/>
 *     <enumeration value="CBB"/>
 *     <enumeration value="CD"/>
 *     <enumeration value="CEC"/>
 *     <enumeration value="CED"/>
 *     <enumeration value="CFE"/>
 *     <enumeration value="CFF"/>
 *     <enumeration value="CFO"/>
 *     <enumeration value="CG"/>
 *     <enumeration value="CH"/>
 *     <enumeration value="CK"/>
 *     <enumeration value="CKN"/>
 *     <enumeration value="CM"/>
 *     <enumeration value="CMR"/>
 *     <enumeration value="CN"/>
 *     <enumeration value="CNO"/>
 *     <enumeration value="COF"/>
 *     <enumeration value="CP"/>
 *     <enumeration value="CR"/>
 *     <enumeration value="CRN"/>
 *     <enumeration value="CS"/>
 *     <enumeration value="CST"/>
 *     <enumeration value="CT"/>
 *     <enumeration value="CU"/>
 *     <enumeration value="CV"/>
 *     <enumeration value="CW"/>
 *     <enumeration value="CZ"/>
 *     <enumeration value="DA"/>
 *     <enumeration value="DAN"/>
 *     <enumeration value="DB"/>
 *     <enumeration value="DI"/>
 *     <enumeration value="DL"/>
 *     <enumeration value="DM"/>
 *     <enumeration value="DQ"/>
 *     <enumeration value="DR"/>
 *     <enumeration value="EA"/>
 *     <enumeration value="EB"/>
 *     <enumeration value="ED"/>
 *     <enumeration value="EE"/>
 *     <enumeration value="EEP"/>
 *     <enumeration value="EI"/>
 *     <enumeration value="EN"/>
 *     <enumeration value="EQ"/>
 *     <enumeration value="ER"/>
 *     <enumeration value="ERN"/>
 *     <enumeration value="ET"/>
 *     <enumeration value="EX"/>
 *     <enumeration value="FC"/>
 *     <enumeration value="FF"/>
 *     <enumeration value="FI"/>
 *     <enumeration value="FLW"/>
 *     <enumeration value="FN"/>
 *     <enumeration value="FO"/>
 *     <enumeration value="FS"/>
 *     <enumeration value="FT"/>
 *     <enumeration value="FV"/>
 *     <enumeration value="FX"/>
 *     <enumeration value="GA"/>
 *     <enumeration value="GC"/>
 *     <enumeration value="GD"/>
 *     <enumeration value="GDN"/>
 *     <enumeration value="GN"/>
 *     <enumeration value="HS"/>
 *     <enumeration value="HWB"/>
 *     <enumeration value="IA"/>
 *     <enumeration value="IB"/>
 *     <enumeration value="ICA"/>
 *     <enumeration value="ICE"/>
 *     <enumeration value="ICO"/>
 *     <enumeration value="II"/>
 *     <enumeration value="IL"/>
 *     <enumeration value="INB"/>
 *     <enumeration value="INN"/>
 *     <enumeration value="INO"/>
 *     <enumeration value="IP"/>
 *     <enumeration value="IS"/>
 *     <enumeration value="IT"/>
 *     <enumeration value="IV"/>
 *     <enumeration value="JB"/>
 *     <enumeration value="JE"/>
 *     <enumeration value="LA"/>
 *     <enumeration value="LAN"/>
 *     <enumeration value="LAR"/>
 *     <enumeration value="LB"/>
 *     <enumeration value="LC"/>
 *     <enumeration value="LI"/>
 *     <enumeration value="LO"/>
 *     <enumeration value="LRC"/>
 *     <enumeration value="LS"/>
 *     <enumeration value="MA"/>
 *     <enumeration value="MB"/>
 *     <enumeration value="MF"/>
 *     <enumeration value="MG"/>
 *     <enumeration value="MH"/>
 *     <enumeration value="MR"/>
 *     <enumeration value="MRN"/>
 *     <enumeration value="MS"/>
 *     <enumeration value="MSS"/>
 *     <enumeration value="MWB"/>
 *     <enumeration value="NA"/>
 *     <enumeration value="NF"/>
 *     <enumeration value="OH"/>
 *     <enumeration value="OI"/>
 *     <enumeration value="ON"/>
 *     <enumeration value="OP"/>
 *     <enumeration value="OR"/>
 *     <enumeration value="PB"/>
 *     <enumeration value="PC"/>
 *     <enumeration value="PD"/>
 *     <enumeration value="PE"/>
 *     <enumeration value="PF"/>
 *     <enumeration value="PI"/>
 *     <enumeration value="PK"/>
 *     <enumeration value="PL"/>
 *     <enumeration value="POR"/>
 *     <enumeration value="PP"/>
 *     <enumeration value="PQ"/>
 *     <enumeration value="PR"/>
 *     <enumeration value="PS"/>
 *     <enumeration value="PW"/>
 *     <enumeration value="PY"/>
 *     <enumeration value="RA"/>
 *     <enumeration value="RC"/>
 *     <enumeration value="RCN"/>
 *     <enumeration value="RE"/>
 *     <enumeration value="REN"/>
 *     <enumeration value="RF"/>
 *     <enumeration value="RR"/>
 *     <enumeration value="RT"/>
 *     <enumeration value="SA"/>
 *     <enumeration value="SB"/>
 *     <enumeration value="SD"/>
 *     <enumeration value="SE"/>
 *     <enumeration value="SEA"/>
 *     <enumeration value="SF"/>
 *     <enumeration value="SH"/>
 *     <enumeration value="SI"/>
 *     <enumeration value="SM"/>
 *     <enumeration value="SN"/>
 *     <enumeration value="SP"/>
 *     <enumeration value="SQ"/>
 *     <enumeration value="SRN"/>
 *     <enumeration value="SS"/>
 *     <enumeration value="STA"/>
 *     <enumeration value="SW"/>
 *     <enumeration value="SZ"/>
 *     <enumeration value="TB"/>
 *     <enumeration value="TCR"/>
 *     <enumeration value="TE"/>
 *     <enumeration value="TF"/>
 *     <enumeration value="TI"/>
 *     <enumeration value="TIN"/>
 *     <enumeration value="TL"/>
 *     <enumeration value="TN"/>
 *     <enumeration value="TP"/>
 *     <enumeration value="UAR"/>
 *     <enumeration value="UC"/>
 *     <enumeration value="UCN"/>
 *     <enumeration value="UN"/>
 *     <enumeration value="UO"/>
 *     <enumeration value="URI"/>
 *     <enumeration value="VA"/>
 *     <enumeration value="VC"/>
 *     <enumeration value="VGR"/>
 *     <enumeration value="VM"/>
 *     <enumeration value="VN"/>
 *     <enumeration value="VON"/>
 *     <enumeration value="VOR"/>
 *     <enumeration value="VP"/>
 *     <enumeration value="VR"/>
 *     <enumeration value="VS"/>
 *     <enumeration value="VT"/>
 *     <enumeration value="VV"/>
 *     <enumeration value="WE"/>
 *     <enumeration value="WM"/>
 *     <enumeration value="WN"/>
 *     <enumeration value="WR"/>
 *     <enumeration value="WS"/>
 *     <enumeration value="WY"/>
 *     <enumeration value="XA"/>
 *     <enumeration value="XC"/>
 *     <enumeration value="XP"/>
 *     <enumeration value="ZZZ"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ReferenceCodeContentType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100")
@XmlEnum
public enum ReferenceCodeContentType {

    AAA,
    AAB,
    AAC,
    AAD,
    AAE,
    AAF,
    AAG,
    AAH,
    AAI,
    AAJ,
    AAK,
    AAL,
    AAM,
    AAN,
    AAO,
    AAP,
    AAQ,
    AAR,
    AAS,
    AAT,
    AAU,
    AAV,
    AAW,
    AAX,
    AAY,
    AAZ,
    ABA,
    ABB,
    ABC,
    ABD,
    ABE,
    ABF,
    ABG,
    ABH,
    ABI,
    ABJ,
    ABK,
    ABL,
    ABM,
    ABN,
    ABO,
    ABP,
    ABQ,
    ABR,
    ABS,
    ABT,
    ABU,
    ABV,
    ABW,
    ABX,
    ABY,
    ABZ,
    AC,
    ACA,
    ACB,
    ACC,
    ACD,
    ACE,
    ACF,
    ACG,
    ACH,
    ACI,
    ACJ,
    ACK,
    ACL,
    ACN,
    ACO,
    ACP,
    ACQ,
    ACR,
    ACT,
    ACU,
    ACV,
    ACW,
    ACX,
    ACY,
    ACZ,
    ADA,
    ADB,
    ADC,
    ADD,
    ADE,
    ADF,
    ADG,
    ADI,
    ADJ,
    ADK,
    ADL,
    ADM,
    ADN,
    ADO,
    ADP,
    ADQ,
    ADT,
    ADU,
    ADV,
    ADW,
    ADX,
    ADY,
    ADZ,
    AE,
    AEA,
    AEB,
    AEC,
    AED,
    AEE,
    AEF,
    AEG,
    AEH,
    AEI,
    AEJ,
    AEK,
    AEL,
    AEM,
    AEN,
    AEO,
    AEP,
    AEQ,
    AER,
    AES,
    AET,
    AEU,
    AEV,
    AEW,
    AEX,
    AEY,
    AEZ,
    AF,
    AFA,
    AFB,
    AFC,
    AFD,
    AFE,
    AFF,
    AFG,
    AFH,
    AFI,
    AFJ,
    AFK,
    AFL,
    AFM,
    AFN,
    AFO,
    AFP,
    AFQ,
    AFR,
    AFS,
    AFT,
    AFU,
    AFV,
    AFW,
    AFX,
    AFY,
    AFZ,
    AGA,
    AGB,
    AGC,
    AGD,
    AGE,
    AGF,
    AGG,
    AGH,
    AGI,
    AGJ,
    AGK,
    AGL,
    AGM,
    AGN,
    AGO,
    AGP,
    AGQ,
    AGR,
    AGS,
    AGT,
    AGU,
    AGV,
    AGW,
    AGX,
    AGY,
    AGZ,
    AHA,
    AHB,
    AHC,
    AHD,
    AHE,
    AHF,
    AHG,
    AHH,
    AHI,
    AHJ,
    AHK,
    AHL,
    AHM,
    AHN,
    AHO,
    AHP,
    AHQ,
    AHR,
    AHS,
    AHT,
    AHU,
    AHV,
    AHX,
    AHY,
    AHZ,
    AIA,
    AIB,
    AIC,
    AID,
    AIE,
    AIF,
    AIG,
    AIH,
    AII,
    AIJ,
    AIK,
    AIL,
    AIM,
    AIN,
    AIO,
    AIP,
    AIQ,
    AIR,
    AIS,
    AIT,
    AIU,
    AIV,
    AIW,
    AIX,
    AIY,
    AIZ,
    AJA,
    AJB,
    AJC,
    AJD,
    AJE,
    AJF,
    AJG,
    AJH,
    AJI,
    AJJ,
    AJK,
    AJL,
    AJM,
    AJN,
    AJO,
    AJP,
    AJQ,
    AJR,
    AJS,
    AJT,
    AJU,
    AJV,
    AJW,
    AJX,
    AJY,
    AJZ,
    AKA,
    AKB,
    AKC,
    AKD,
    AKE,
    AKF,
    AKG,
    AKH,
    AKI,
    AKJ,
    AKK,
    AKL,
    AKM,
    AKN,
    AKO,
    AKP,
    AKQ,
    AKR,
    AKS,
    AKT,
    AKU,
    AKV,
    AKW,
    AKX,
    AKY,
    AKZ,
    ALA,
    ALB,
    ALC,
    ALD,
    ALE,
    ALF,
    ALG,
    ALH,
    ALI,
    ALJ,
    ALK,
    ALL,
    ALM,
    ALN,
    ALO,
    ALP,
    ALQ,
    ALR,
    ALS,
    ALT,
    ALU,
    ALV,
    ALW,
    ALX,
    ALY,
    ALZ,
    AMA,
    AMB,
    AMC,
    AMD,
    AME,
    AMF,
    AMG,
    AMH,
    AMI,
    AMJ,
    AMK,
    AML,
    AMM,
    AMN,
    AMO,
    AMP,
    AMQ,
    AMR,
    AMS,
    AMT,
    AMU,
    AMV,
    AMW,
    AMX,
    AMY,
    AMZ,
    ANA,
    ANB,
    ANC,
    AND,
    ANE,
    ANF,
    ANG,
    ANH,
    ANI,
    ANJ,
    ANK,
    ANL,
    ANM,
    ANN,
    ANO,
    ANP,
    ANQ,
    ANR,
    ANS,
    ANT,
    ANU,
    ANV,
    ANW,
    ANX,
    ANY,
    AOA,
    AOD,
    AOE,
    AOF,
    AOG,
    AOH,
    AOI,
    AOJ,
    AOK,
    AOL,
    AOM,
    AON,
    AOO,
    AOP,
    AOQ,
    AOR,
    AOS,
    AOT,
    AOU,
    AOV,
    AOW,
    AOX,
    AOY,
    AOZ,
    AP,
    APA,
    APB,
    APC,
    APD,
    APE,
    APF,
    APG,
    APH,
    API,
    APJ,
    APK,
    APL,
    APM,
    APN,
    APO,
    APP,
    APQ,
    APR,
    APS,
    APT,
    APU,
    APV,
    APW,
    APX,
    APY,
    APZ,
    AQA,
    AQB,
    AQC,
    AQD,
    AQE,
    AQF,
    AQG,
    AQH,
    AQI,
    AQJ,
    AQK,
    AQL,
    AQM,
    AQN,
    AQO,
    AQP,
    AQQ,
    AQR,
    AQS,
    AQT,
    AQU,
    AQV,
    AQW,
    AQX,
    AQY,
    AQZ,
    ARA,
    ARB,
    ARC,
    ARD,
    ARE,
    ARF,
    ARG,
    ARH,
    ARI,
    ARJ,
    ARK,
    ARL,
    ARM,
    ARN,
    ARO,
    ARP,
    ARQ,
    ARR,
    ARS,
    ART,
    ARU,
    ARV,
    ARW,
    ARX,
    ARY,
    ARZ,
    ASA,
    ASB,
    ASC,
    ASD,
    ASE,
    ASF,
    ASG,
    ASH,
    ASI,
    ASJ,
    ASK,
    ASL,
    ASM,
    ASN,
    ASO,
    ASP,
    ASQ,
    ASR,
    ASS,
    AST,
    ASU,
    ASV,
    ASW,
    ASX,
    ASY,
    ASZ,
    ATA,
    ATB,
    ATC,
    ATD,
    ATE,
    ATF,
    ATG,
    ATH,
    ATI,
    ATJ,
    ATK,
    ATL,
    ATM,
    ATN,
    ATO,
    ATP,
    ATQ,
    ATR,
    ATS,
    ATT,
    ATU,
    ATV,
    ATW,
    ATX,
    ATY,
    ATZ,
    AU,
    AUA,
    AUB,
    AUC,
    AUD,
    AUE,
    AUF,
    AUG,
    AUH,
    AUI,
    AUJ,
    AUK,
    AUL,
    AUM,
    AUN,
    AUO,
    AUP,
    AUQ,
    AUR,
    AUS,
    AUT,
    AUU,
    AUV,
    AUW,
    AUX,
    AUY,
    AUZ,
    AV,
    AVA,
    AVB,
    AVC,
    AVD,
    AVE,
    AVF,
    AVG,
    AVH,
    AVI,
    AVJ,
    AVK,
    AVL,
    AVM,
    AVN,
    AVO,
    AVP,
    AVQ,
    AVR,
    AVS,
    AVT,
    AVU,
    AVV,
    AVW,
    AVX,
    AVY,
    AVZ,
    AWA,
    AWB,
    AWC,
    AWD,
    AWE,
    AWF,
    AWG,
    AWH,
    AWI,
    AWJ,
    AWK,
    AWL,
    AWM,
    AWN,
    AWO,
    AWP,
    AWQ,
    AWR,
    AWS,
    AWT,
    AWU,
    AWV,
    AWW,
    AWX,
    AWY,
    AWZ,
    AXA,
    AXB,
    AXC,
    AXD,
    AXE,
    AXF,
    AXG,
    AXH,
    AXI,
    AXJ,
    AXK,
    AXL,
    AXM,
    AXN,
    AXO,
    AXP,
    AXQ,
    AXR,
    AXS,
    BA,
    BC,
    BD,
    BE,
    BH,
    BM,
    BN,
    BO,
    BR,
    BT,
    BTP,
    BW,
    CAS,
    CAT,
    CAU,
    CAV,
    CAW,
    CAX,
    CAY,
    CAZ,
    CBA,
    CBB,
    CD,
    CEC,
    CED,
    CFE,
    CFF,
    CFO,
    CG,
    CH,
    CK,
    CKN,
    CM,
    CMR,
    CN,
    CNO,
    COF,
    CP,
    CR,
    CRN,
    CS,
    CST,
    CT,
    CU,
    CV,
    CW,
    CZ,
    DA,
    DAN,
    DB,
    DI,
    DL,
    DM,
    DQ,
    DR,
    EA,
    EB,
    ED,
    EE,
    EEP,
    EI,
    EN,
    EQ,
    ER,
    ERN,
    ET,
    EX,
    FC,
    FF,
    FI,
    FLW,
    FN,
    FO,
    FS,
    FT,
    FV,
    FX,
    GA,
    GC,
    GD,
    GDN,
    GN,
    HS,
    HWB,
    IA,
    IB,
    ICA,
    ICE,
    ICO,
    II,
    IL,
    INB,
    INN,
    INO,
    IP,
    IS,
    IT,
    IV,
    JB,
    JE,
    LA,
    LAN,
    LAR,
    LB,
    LC,
    LI,
    LO,
    LRC,
    LS,
    MA,
    MB,
    MF,
    MG,
    MH,
    MR,
    MRN,
    MS,
    MSS,
    MWB,
    NA,
    NF,
    OH,
    OI,
    ON,
    OP,
    OR,
    PB,
    PC,
    PD,
    PE,
    PF,
    PI,
    PK,
    PL,
    POR,
    PP,
    PQ,
    PR,
    PS,
    PW,
    PY,
    RA,
    RC,
    RCN,
    RE,
    REN,
    RF,
    RR,
    RT,
    SA,
    SB,
    SD,
    SE,
    SEA,
    SF,
    SH,
    SI,
    SM,
    SN,
    SP,
    SQ,
    SRN,
    SS,
    STA,
    SW,
    SZ,
    TB,
    TCR,
    TE,
    TF,
    TI,
    TIN,
    TL,
    TN,
    TP,
    UAR,
    UC,
    UCN,
    UN,
    UO,
    URI,
    VA,
    VC,
    VGR,
    VM,
    VN,
    VON,
    VOR,
    VP,
    VR,
    VS,
    VT,
    VV,
    WE,
    WM,
    WN,
    WR,
    WS,
    WY,
    XA,
    XC,
    XP,
    ZZZ;

    public String value() {
        return name();
    }

    public static ReferenceCodeContentType fromValue(String v) {
        return valueOf(v);
    }

}
