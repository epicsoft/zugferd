
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradeProductInstanceType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TradeProductInstanceType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="BatchID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="SupplierAssignedSerialID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeProductInstanceType", propOrder = {
    "batchID",
    "supplierAssignedSerialID"
})
public class TradeProductInstanceType {

    @XmlElement(name = "BatchID")
    protected IDType batchID;
    @XmlElement(name = "SupplierAssignedSerialID")
    protected IDType supplierAssignedSerialID;

    /**
     * Gets the value of the batchID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getBatchID() {
        return batchID;
    }

    /**
     * Sets the value of the batchID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setBatchID(IDType value) {
        this.batchID = value;
    }

    /**
     * Gets the value of the supplierAssignedSerialID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getSupplierAssignedSerialID() {
        return supplierAssignedSerialID;
    }

    /**
     * Sets the value of the supplierAssignedSerialID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setSupplierAssignedSerialID(IDType value) {
        this.supplierAssignedSerialID = value;
    }

}
