
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReferencedDocumentType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="ReferencedDocumentType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="IssuerAssignedID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="URIID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="LineID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/>
 *         <element name="TypeCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}DocumentCodeType" minOccurs="0"/>
 *         <element name="Name" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="AttachmentBinaryObject" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}BinaryObjectType" minOccurs="0"/>
 *         <element name="ReferenceTypeCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}ReferenceCodeType" minOccurs="0"/>
 *         <element name="FormattedIssueDateTime" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}FormattedDateTimeType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferencedDocumentType", propOrder = {
    "issuerAssignedID",
    "uriid",
    "lineID",
    "typeCode",
    "names",
    "attachmentBinaryObject",
    "referenceTypeCode",
    "formattedIssueDateTime"
})
public class ReferencedDocumentType {

    @XmlElement(name = "IssuerAssignedID")
    protected IDType issuerAssignedID;
    @XmlElement(name = "URIID")
    protected IDType uriid;
    @XmlElement(name = "LineID")
    protected IDType lineID;
    @XmlElement(name = "TypeCode")
    protected DocumentCodeType typeCode;
    @XmlElement(name = "Name")
    protected List<TextType> names;
    @XmlElement(name = "AttachmentBinaryObject")
    protected BinaryObjectType attachmentBinaryObject;
    @XmlElement(name = "ReferenceTypeCode")
    protected ReferenceCodeType referenceTypeCode;
    @XmlElement(name = "FormattedIssueDateTime")
    protected FormattedDateTimeType formattedIssueDateTime;

    /**
     * Gets the value of the issuerAssignedID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getIssuerAssignedID() {
        return issuerAssignedID;
    }

    /**
     * Sets the value of the issuerAssignedID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setIssuerAssignedID(IDType value) {
        this.issuerAssignedID = value;
    }

    /**
     * Gets the value of the uriid property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getURIID() {
        return uriid;
    }

    /**
     * Sets the value of the uriid property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setURIID(IDType value) {
        this.uriid = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setLineID(IDType value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentCodeType }
     *     
     */
    public DocumentCodeType getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentCodeType }
     *     
     */
    public void setTypeCode(DocumentCodeType value) {
        this.typeCode = value;
    }

    /**
     * Gets the value of the names property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the names property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * </p>
     * 
     * 
     * @return
     *     The value of the names property.
     */
    public List<TextType> getNames() {
        if (names == null) {
            names = new ArrayList<>();
        }
        return this.names;
    }

    /**
     * Gets the value of the attachmentBinaryObject property.
     * 
     * @return
     *     possible object is
     *     {@link BinaryObjectType }
     *     
     */
    public BinaryObjectType getAttachmentBinaryObject() {
        return attachmentBinaryObject;
    }

    /**
     * Sets the value of the attachmentBinaryObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link BinaryObjectType }
     *     
     */
    public void setAttachmentBinaryObject(BinaryObjectType value) {
        this.attachmentBinaryObject = value;
    }

    /**
     * Gets the value of the referenceTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceCodeType }
     *     
     */
    public ReferenceCodeType getReferenceTypeCode() {
        return referenceTypeCode;
    }

    /**
     * Sets the value of the referenceTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceCodeType }
     *     
     */
    public void setReferenceTypeCode(ReferenceCodeType value) {
        this.referenceTypeCode = value;
    }

    /**
     * Gets the value of the formattedIssueDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link FormattedDateTimeType }
     *     
     */
    public FormattedDateTimeType getFormattedIssueDateTime() {
        return formattedIssueDateTime;
    }

    /**
     * Sets the value of the formattedIssueDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormattedDateTimeType }
     *     
     */
    public void setFormattedIssueDateTime(FormattedDateTimeType value) {
        this.formattedIssueDateTime = value;
    }

}
