
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SupplyChainTradeTransactionType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="SupplyChainTradeTransactionType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="IncludedSupplyChainTradeLineItem" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SupplyChainTradeLineItemType" maxOccurs="unbounded"/>
 *         <element name="ApplicableHeaderTradeAgreement" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}HeaderTradeAgreementType"/>
 *         <element name="ApplicableHeaderTradeDelivery" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}HeaderTradeDeliveryType"/>
 *         <element name="ApplicableHeaderTradeSettlement" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}HeaderTradeSettlementType"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupplyChainTradeTransactionType", propOrder = {
    "includedSupplyChainTradeLineItems",
    "applicableHeaderTradeAgreement",
    "applicableHeaderTradeDelivery",
    "applicableHeaderTradeSettlement"
})
public class SupplyChainTradeTransactionType {

    @XmlElement(name = "IncludedSupplyChainTradeLineItem", required = true)
    protected List<SupplyChainTradeLineItemType> includedSupplyChainTradeLineItems;
    @XmlElement(name = "ApplicableHeaderTradeAgreement", required = true)
    protected HeaderTradeAgreementType applicableHeaderTradeAgreement;
    @XmlElement(name = "ApplicableHeaderTradeDelivery", required = true)
    protected HeaderTradeDeliveryType applicableHeaderTradeDelivery;
    @XmlElement(name = "ApplicableHeaderTradeSettlement", required = true)
    protected HeaderTradeSettlementType applicableHeaderTradeSettlement;

    /**
     * Gets the value of the includedSupplyChainTradeLineItems property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedSupplyChainTradeLineItems property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getIncludedSupplyChainTradeLineItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupplyChainTradeLineItemType }
     * </p>
     * 
     * 
     * @return
     *     The value of the includedSupplyChainTradeLineItems property.
     */
    public List<SupplyChainTradeLineItemType> getIncludedSupplyChainTradeLineItems() {
        if (includedSupplyChainTradeLineItems == null) {
            includedSupplyChainTradeLineItems = new ArrayList<>();
        }
        return this.includedSupplyChainTradeLineItems;
    }

    /**
     * Gets the value of the applicableHeaderTradeAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderTradeAgreementType }
     *     
     */
    public HeaderTradeAgreementType getApplicableHeaderTradeAgreement() {
        return applicableHeaderTradeAgreement;
    }

    /**
     * Sets the value of the applicableHeaderTradeAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderTradeAgreementType }
     *     
     */
    public void setApplicableHeaderTradeAgreement(HeaderTradeAgreementType value) {
        this.applicableHeaderTradeAgreement = value;
    }

    /**
     * Gets the value of the applicableHeaderTradeDelivery property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderTradeDeliveryType }
     *     
     */
    public HeaderTradeDeliveryType getApplicableHeaderTradeDelivery() {
        return applicableHeaderTradeDelivery;
    }

    /**
     * Sets the value of the applicableHeaderTradeDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderTradeDeliveryType }
     *     
     */
    public void setApplicableHeaderTradeDelivery(HeaderTradeDeliveryType value) {
        this.applicableHeaderTradeDelivery = value;
    }

    /**
     * Gets the value of the applicableHeaderTradeSettlement property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderTradeSettlementType }
     *     
     */
    public HeaderTradeSettlementType getApplicableHeaderTradeSettlement() {
        return applicableHeaderTradeSettlement;
    }

    /**
     * Sets the value of the applicableHeaderTradeSettlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderTradeSettlementType }
     *     
     */
    public void setApplicableHeaderTradeSettlement(HeaderTradeSettlementType value) {
        this.applicableHeaderTradeSettlement = value;
    }

}
