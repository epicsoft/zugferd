
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for CurrencyCodeType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="CurrencyCodeType">
 *   <simpleContent>
 *     <extension base="<urn:un:unece:uncefact:data:standard:QualifiedDataType:100>CurrencyCodeContentType">
 *     </extension>
 *   </simpleContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrencyCodeType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100", propOrder = {
    "value"
})
public class CurrencyCodeType {

    @XmlValue
    protected CurrencyCodeContentType value;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeContentType }
     *     
     */
    public CurrencyCodeContentType getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeContentType }
     *     
     */
    public void setValue(CurrencyCodeContentType value) {
        this.value = value;
    }

}
