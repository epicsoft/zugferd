
package one.epicsoft.zugferd.extended;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradeSettlementLineMonetarySummationType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="TradeSettlementLineMonetarySummationType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="LineTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType"/>
 *         <element name="ChargeTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="AllowanceTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="TaxTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="GrandTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *         <element name="TotalAllowanceChargeAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeSettlementLineMonetarySummationType", propOrder = {
    "lineTotalAmount",
    "chargeTotalAmount",
    "allowanceTotalAmount",
    "taxTotalAmount",
    "grandTotalAmount",
    "totalAllowanceChargeAmount"
})
public class TradeSettlementLineMonetarySummationType {

    @XmlElement(name = "LineTotalAmount", required = true)
    protected AmountType lineTotalAmount;
    @XmlElement(name = "ChargeTotalAmount")
    protected AmountType chargeTotalAmount;
    @XmlElement(name = "AllowanceTotalAmount")
    protected AmountType allowanceTotalAmount;
    @XmlElement(name = "TaxTotalAmount")
    protected AmountType taxTotalAmount;
    @XmlElement(name = "GrandTotalAmount")
    protected AmountType grandTotalAmount;
    @XmlElement(name = "TotalAllowanceChargeAmount")
    protected AmountType totalAllowanceChargeAmount;

    /**
     * Gets the value of the lineTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getLineTotalAmount() {
        return lineTotalAmount;
    }

    /**
     * Sets the value of the lineTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setLineTotalAmount(AmountType value) {
        this.lineTotalAmount = value;
    }

    /**
     * Gets the value of the chargeTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getChargeTotalAmount() {
        return chargeTotalAmount;
    }

    /**
     * Sets the value of the chargeTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setChargeTotalAmount(AmountType value) {
        this.chargeTotalAmount = value;
    }

    /**
     * Gets the value of the allowanceTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getAllowanceTotalAmount() {
        return allowanceTotalAmount;
    }

    /**
     * Sets the value of the allowanceTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setAllowanceTotalAmount(AmountType value) {
        this.allowanceTotalAmount = value;
    }

    /**
     * Gets the value of the taxTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getTaxTotalAmount() {
        return taxTotalAmount;
    }

    /**
     * Sets the value of the taxTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setTaxTotalAmount(AmountType value) {
        this.taxTotalAmount = value;
    }

    /**
     * Gets the value of the grandTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getGrandTotalAmount() {
        return grandTotalAmount;
    }

    /**
     * Sets the value of the grandTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setGrandTotalAmount(AmountType value) {
        this.grandTotalAmount = value;
    }

    /**
     * Gets the value of the totalAllowanceChargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getTotalAllowanceChargeAmount() {
        return totalAllowanceChargeAmount;
    }

    /**
     * Sets the value of the totalAllowanceChargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setTotalAllowanceChargeAmount(AmountType value) {
        this.totalAllowanceChargeAmount = value;
    }

}
