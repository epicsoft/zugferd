
package one.epicsoft.zugferd.extended;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LineTradeSettlementType complex type</p>.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.</p>
 * 
 * <pre>{@code
 * <complexType name="LineTradeSettlementType">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="ApplicableTradeTax" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeTaxType" maxOccurs="unbounded"/>
 *         <element name="BillingSpecifiedPeriod" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SpecifiedPeriodType" minOccurs="0"/>
 *         <element name="SpecifiedTradeAllowanceCharge" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAllowanceChargeType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="SpecifiedTradeSettlementLineMonetarySummation" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeSettlementLineMonetarySummationType"/>
 *         <element name="InvoiceReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/>
 *         <element name="AdditionalReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="ReceivableSpecifiedTradeAccountingAccount" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAccountingAccountType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineTradeSettlementType", propOrder = {
    "applicableTradeTaxes",
    "billingSpecifiedPeriod",
    "specifiedTradeAllowanceCharges",
    "specifiedTradeSettlementLineMonetarySummation",
    "invoiceReferencedDocument",
    "additionalReferencedDocuments",
    "receivableSpecifiedTradeAccountingAccount"
})
public class LineTradeSettlementType {

    @XmlElement(name = "ApplicableTradeTax", required = true)
    protected List<TradeTaxType> applicableTradeTaxes;
    @XmlElement(name = "BillingSpecifiedPeriod")
    protected SpecifiedPeriodType billingSpecifiedPeriod;
    @XmlElement(name = "SpecifiedTradeAllowanceCharge")
    protected List<TradeAllowanceChargeType> specifiedTradeAllowanceCharges;
    @XmlElement(name = "SpecifiedTradeSettlementLineMonetarySummation", required = true)
    protected TradeSettlementLineMonetarySummationType specifiedTradeSettlementLineMonetarySummation;
    @XmlElement(name = "InvoiceReferencedDocument")
    protected ReferencedDocumentType invoiceReferencedDocument;
    @XmlElement(name = "AdditionalReferencedDocument")
    protected List<ReferencedDocumentType> additionalReferencedDocuments;
    @XmlElement(name = "ReceivableSpecifiedTradeAccountingAccount")
    protected TradeAccountingAccountType receivableSpecifiedTradeAccountingAccount;

    /**
     * Gets the value of the applicableTradeTaxes property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableTradeTaxes property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getApplicableTradeTaxes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeTaxType }
     * </p>
     * 
     * 
     * @return
     *     The value of the applicableTradeTaxes property.
     */
    public List<TradeTaxType> getApplicableTradeTaxes() {
        if (applicableTradeTaxes == null) {
            applicableTradeTaxes = new ArrayList<>();
        }
        return this.applicableTradeTaxes;
    }

    /**
     * Gets the value of the billingSpecifiedPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link SpecifiedPeriodType }
     *     
     */
    public SpecifiedPeriodType getBillingSpecifiedPeriod() {
        return billingSpecifiedPeriod;
    }

    /**
     * Sets the value of the billingSpecifiedPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecifiedPeriodType }
     *     
     */
    public void setBillingSpecifiedPeriod(SpecifiedPeriodType value) {
        this.billingSpecifiedPeriod = value;
    }

    /**
     * Gets the value of the specifiedTradeAllowanceCharges property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTradeAllowanceCharges property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getSpecifiedTradeAllowanceCharges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeAllowanceChargeType }
     * </p>
     * 
     * 
     * @return
     *     The value of the specifiedTradeAllowanceCharges property.
     */
    public List<TradeAllowanceChargeType> getSpecifiedTradeAllowanceCharges() {
        if (specifiedTradeAllowanceCharges == null) {
            specifiedTradeAllowanceCharges = new ArrayList<>();
        }
        return this.specifiedTradeAllowanceCharges;
    }

    /**
     * Gets the value of the specifiedTradeSettlementLineMonetarySummation property.
     * 
     * @return
     *     possible object is
     *     {@link TradeSettlementLineMonetarySummationType }
     *     
     */
    public TradeSettlementLineMonetarySummationType getSpecifiedTradeSettlementLineMonetarySummation() {
        return specifiedTradeSettlementLineMonetarySummation;
    }

    /**
     * Sets the value of the specifiedTradeSettlementLineMonetarySummation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeSettlementLineMonetarySummationType }
     *     
     */
    public void setSpecifiedTradeSettlementLineMonetarySummation(TradeSettlementLineMonetarySummationType value) {
        this.specifiedTradeSettlementLineMonetarySummation = value;
    }

    /**
     * Gets the value of the invoiceReferencedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getInvoiceReferencedDocument() {
        return invoiceReferencedDocument;
    }

    /**
     * Sets the value of the invoiceReferencedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setInvoiceReferencedDocument(ReferencedDocumentType value) {
        this.invoiceReferencedDocument = value;
    }

    /**
     * Gets the value of the additionalReferencedDocuments property.
     * 
     * <p>This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalReferencedDocuments property.</p>
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * </p>
     * <pre>
     * getAdditionalReferencedDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentType }
     * </p>
     * 
     * 
     * @return
     *     The value of the additionalReferencedDocuments property.
     */
    public List<ReferencedDocumentType> getAdditionalReferencedDocuments() {
        if (additionalReferencedDocuments == null) {
            additionalReferencedDocuments = new ArrayList<>();
        }
        return this.additionalReferencedDocuments;
    }

    /**
     * Gets the value of the receivableSpecifiedTradeAccountingAccount property.
     * 
     * @return
     *     possible object is
     *     {@link TradeAccountingAccountType }
     *     
     */
    public TradeAccountingAccountType getReceivableSpecifiedTradeAccountingAccount() {
        return receivableSpecifiedTradeAccountingAccount;
    }

    /**
     * Sets the value of the receivableSpecifiedTradeAccountingAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeAccountingAccountType }
     *     
     */
    public void setReceivableSpecifiedTradeAccountingAccount(TradeAccountingAccountType value) {
        this.receivableSpecifiedTradeAccountingAccount = value;
    }

}
