package one.epicsoft.zugferd.builders;

import one.epicsoft.zugferd.extended.TradePartyType;

public class Buyer extends TradeParty {

    private Buyer() {
        this.doc = new TradePartyType();
    }

    public static Buyer builder() {
        return new Buyer();
    }

    @Override
    public Buyer name(final String name) {
        super.name(name);
        return this;
    }

    @Override
    public Buyer zipCode(final String zipCode) {
        super.zipCode(zipCode);
        return this;
    }

    @Override
    public Buyer streetWithHouseNumber(final String streetWithHouseNo) {
        super.streetWithHouseNumber(streetWithHouseNo);
        return this;
    }

    @Override
    public Buyer city(final String city) {
        super.city(city);
        return this;
    }

    @Override
    public Buyer countryIso2(final String countryIso2) {
        super.countryIso2(countryIso2);
        return this;
    }
}
