package one.epicsoft.zugferd.builders;

import static java.util.Objects.isNull;

import java.util.List;

import one.epicsoft.zugferd.exceptions.NotImplementedException;
import one.epicsoft.zugferd.extended.CrossIndustryInvoice;
import one.epicsoft.zugferd.extended.DateTimeType;
import one.epicsoft.zugferd.extended.DateTimeType.DateTimeString;
import one.epicsoft.zugferd.extended.DocumentCodeType;
import one.epicsoft.zugferd.extended.DocumentContextParameterType;
import one.epicsoft.zugferd.extended.ExchangedDocumentContextType;
import one.epicsoft.zugferd.extended.ExchangedDocumentType;
import one.epicsoft.zugferd.extended.HeaderTradeAgreementType;
import one.epicsoft.zugferd.extended.HeaderTradeDeliveryType;
import one.epicsoft.zugferd.extended.IDType;
import one.epicsoft.zugferd.extended.SupplyChainEventType;
import one.epicsoft.zugferd.extended.SupplyChainTradeLineItemType;
import one.epicsoft.zugferd.extended.SupplyChainTradeTransactionType;
import one.epicsoft.zugferd.extended.TradePartyType;

public class Invoice {

    public static final DateTimeFormat DATE_FORMAT = DateTimeFormat.F_102;

    private static final String DEFAULT_SPECIFICATION_IDENTIFIER = "urn:cen.eu:en16931:2017";

    private final CrossIndustryInvoice doc;

    private Invoice(final Type type) {
        if (type == Type.CROSS_INDUSTRY_INVOICE) {
            this.doc = new CrossIndustryInvoice();
        } else {
            throw new NotImplementedException(String.format("Invoice tyoe '%s' not implemented", type));
        }
        this.specificationIdentifier(DEFAULT_SPECIFICATION_IDENTIFIER);
    }

    public static Invoice builder() {
        return builder(Type.CROSS_INDUSTRY_INVOICE);
    }

    public static Invoice builder(final Type type) {
        return new Invoice(type);
    }

    public CrossIndustryInvoice buildCrossIndustryInvoice() {
        return this.doc;
    }

    public Invoice specificationIdentifier(final String sid) {
        if (isNull(this.doc.getExchangedDocumentContext())) {
            this.doc.setExchangedDocumentContext(new ExchangedDocumentContextType());
        }
        if (isNull(this.doc.getExchangedDocumentContext().getGuidelineSpecifiedDocumentContextParameter())) {
            this.doc.getExchangedDocumentContext().setGuidelineSpecifiedDocumentContextParameter(new DocumentContextParameterType());
        }
        if (isNull(this.doc.getExchangedDocumentContext().getGuidelineSpecifiedDocumentContextParameter().getID())) {
            this.doc.getExchangedDocumentContext().getGuidelineSpecifiedDocumentContextParameter().setID(new IDType());
        }
        this.doc.getExchangedDocumentContext().getGuidelineSpecifiedDocumentContextParameter().getID().setValue(sid);
        return this;
    }

    public Invoice invoiceNumber(final String number) {
        if (isNull(this.doc.getExchangedDocument())) {
            this.doc.setExchangedDocument(new ExchangedDocumentType());
        }
        if (isNull(this.doc.getExchangedDocument().getID())) {
            this.doc.getExchangedDocument().setID(new IDType());
        }
        this.doc.getExchangedDocument().getID().setValue(number);
        return this;
    }

    public Invoice type(final InvoiceType type) {
        if (isNull(this.doc.getExchangedDocument())) {
            this.doc.setExchangedDocument(new ExchangedDocumentType());
        }
        if (isNull(this.doc.getExchangedDocument().getTypeCode())) {
            this.doc.getExchangedDocument().setTypeCode(new DocumentCodeType());
        }
        this.doc.getExchangedDocument().getTypeCode().setValue(type.getCode());
        return this;
    }

    public Invoice issueDate(final DateTime issueDate) {
        if (isNull(this.doc.getExchangedDocument())) {
            this.doc.setExchangedDocument(new ExchangedDocumentType());
        }
        if (isNull(this.doc.getExchangedDocument().getIssueDateTime())) {
            this.doc.getExchangedDocument().setIssueDateTime(new DateTimeType());
        }
        final DateTimeString date = new DateTimeString();
        date.setFormat(DATE_FORMAT.getName());
        date.setValue(issueDate.format(DATE_FORMAT));
        this.doc.getExchangedDocument().getIssueDateTime().setDateTimeString(date);
        return this;
    }

    public Invoice occurrenceDate(final DateTime occurrenceDate) {
        if (isNull(this.doc.getSupplyChainTradeTransaction())) {
            this.doc.setSupplyChainTradeTransaction(new SupplyChainTradeTransactionType());
        }
        if (isNull(this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeDelivery())) {
            this.doc.getSupplyChainTradeTransaction().setApplicableHeaderTradeDelivery(new HeaderTradeDeliveryType());
        }
        if (isNull(this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeDelivery().getActualDeliverySupplyChainEvent())) {
            this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeDelivery().setActualDeliverySupplyChainEvent(new SupplyChainEventType());
        }
        if (isNull(this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeDelivery().getActualDeliverySupplyChainEvent().getOccurrenceDateTime())) {
            this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeDelivery().getActualDeliverySupplyChainEvent().setOccurrenceDateTime(new DateTimeType());
        }
        final DateTimeString date = new DateTimeString();
        date.setFormat(DATE_FORMAT.getName());
        date.setValue(occurrenceDate.format(DATE_FORMAT));
        this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeDelivery().getActualDeliverySupplyChainEvent().getOccurrenceDateTime().setDateTimeString(date);
        return this;
    }

    public Invoice buyer(final Buyer buyer) {
        return this.buyer(buyer.build());
    }

    public Invoice buyer(final TradePartyType buyer) {
        if (isNull(this.doc.getSupplyChainTradeTransaction())) {
            this.doc.setSupplyChainTradeTransaction(new SupplyChainTradeTransactionType());
        }
        if (isNull(this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeAgreement())) {
            this.doc.getSupplyChainTradeTransaction().setApplicableHeaderTradeAgreement(new HeaderTradeAgreementType());
        }
        this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeAgreement().setBuyerTradeParty(buyer);
        return this;
    }

    public Invoice seller(final Seller seller) {
        return this.seller(seller.build());
    }

    public Invoice seller(final TradePartyType seller) {
        if (isNull(this.doc.getSupplyChainTradeTransaction())) {
            this.doc.setSupplyChainTradeTransaction(new SupplyChainTradeTransactionType());
        }
        if (isNull(this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeAgreement())) {
            this.doc.getSupplyChainTradeTransaction().setApplicableHeaderTradeAgreement(new HeaderTradeAgreementType());
        }
        this.doc.getSupplyChainTradeTransaction().getApplicableHeaderTradeAgreement().setSellerTradeParty(seller);
        return this;
    }

    public Invoice positions(final List<Position> positions) {
        if (isNull(this.doc.getSupplyChainTradeTransaction())) {
            this.doc.setSupplyChainTradeTransaction(new SupplyChainTradeTransactionType());
        }
        final List<SupplyChainTradeLineItemType> items = this.doc.getSupplyChainTradeTransaction().getIncludedSupplyChainTradeLineItems();
        items.clear();
        positions.forEach(this::addPosition);
        return this;
    }

    public Invoice addPosition(final Position position) {
        if (isNull(this.doc.getSupplyChainTradeTransaction())) {
            this.doc.setSupplyChainTradeTransaction(new SupplyChainTradeTransactionType());
        }
        this.doc.getSupplyChainTradeTransaction().getIncludedSupplyChainTradeLineItems().add(position.build());
        return this;
    }

    public Invoice summary(final Summary summary) {
        if (isNull(this.doc.getSupplyChainTradeTransaction())) {
            this.doc.setSupplyChainTradeTransaction(new SupplyChainTradeTransactionType());
        }
        this.doc.getSupplyChainTradeTransaction().setApplicableHeaderTradeSettlement(summary.build());
        return this;
    }

    public enum Type {
        CROSS_INDUSTRY_INVOICE;
    }
}
