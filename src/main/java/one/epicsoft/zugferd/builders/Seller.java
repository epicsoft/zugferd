package one.epicsoft.zugferd.builders;

import one.epicsoft.zugferd.extended.TradePartyType;

public class Seller extends TradeParty {

    private Seller() {
        this.doc = new TradePartyType();
    }

    public static Seller builder() {
        return new Seller();
    }

    @Override
    public Seller name(final String name) {
        super.name(name);
        return this;
    }

    @Override
    public Seller zipCode(final String zipCode) {
        super.zipCode(zipCode);
        return this;
    }

    @Override
    public Seller streetWithHouseNumber(final String streetWithHouseNo) {
        super.streetWithHouseNumber(streetWithHouseNo);
        return this;
    }

    @Override
    public Seller city(final String city) {
        super.city(city);
        return this;
    }

    @Override
    public Seller countryIso2(final String countryIso2) {
        super.countryIso2(countryIso2);
        return this;
    }
}
