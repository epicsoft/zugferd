package one.epicsoft.zugferd.builders;

import static java.util.Objects.isNull;
import static one.epicsoft.zugferd.helpers.StringUtils.emptyIfBlank;

import java.math.BigDecimal;
import java.util.List;

import one.epicsoft.zugferd.extended.AmountType;
import one.epicsoft.zugferd.extended.DocumentLineDocumentType;
import one.epicsoft.zugferd.extended.IDType;
import one.epicsoft.zugferd.extended.LineTradeAgreementType;
import one.epicsoft.zugferd.extended.LineTradeDeliveryType;
import one.epicsoft.zugferd.extended.LineTradeSettlementType;
import one.epicsoft.zugferd.extended.QuantityType;
import one.epicsoft.zugferd.extended.SupplyChainTradeLineItemType;
import one.epicsoft.zugferd.extended.TextType;
import one.epicsoft.zugferd.extended.TradePriceType;
import one.epicsoft.zugferd.extended.TradeProductType;
import one.epicsoft.zugferd.extended.TradeSettlementLineMonetarySummationType;

public class Position {

    private final SupplyChainTradeLineItemType doc;

    private Position() {
        this.doc = new SupplyChainTradeLineItemType();
    }

    public static Position builder() {
        return new Position();
    }

    public SupplyChainTradeLineItemType build() {
        return this.doc;
    }

    public Position lineId(final String lineId) {
        if (isNull(this.doc.getAssociatedDocumentLineDocument())) {
            this.doc.setAssociatedDocumentLineDocument(new DocumentLineDocumentType());
        }
        final IDType id = new IDType();
        id.setValue(lineId);
        this.doc.getAssociatedDocumentLineDocument().setLineID(id);
        return this;
    }

    public Position number(final String number) {
        if (isNull(this.doc.getSpecifiedTradeProduct())) {
            this.doc.setSpecifiedTradeProduct(new TradeProductType());
        }
        final IDType text = new IDType();
        text.setValue(emptyIfBlank(number));
        this.doc.getSpecifiedTradeProduct().setSellerAssignedID(null);
        return this;
    }

    public Position name(final String name) {
        if (isNull(this.doc.getSpecifiedTradeProduct())) {
            this.doc.setSpecifiedTradeProduct(new TradeProductType());
        }
        final TextType text = new TextType();
        text.setValue(emptyIfBlank(name));
        this.doc.getSpecifiedTradeProduct().setName(text);
        return this;
    }

    public Position description(final String description) {
        if (isNull(this.doc.getSpecifiedTradeProduct())) {
            this.doc.setSpecifiedTradeProduct(new TradeProductType());
        }
        final TextType text = new TextType();
        text.setValue(emptyIfBlank(description));
        this.doc.getSpecifiedTradeProduct().setDescription(text);
        return this;
    }

    public Position netAmount(final BigDecimal netAmount) {
        if (isNull(this.doc.getSpecifiedLineTradeAgreement())) {
            this.doc.setSpecifiedLineTradeAgreement(new LineTradeAgreementType());
        }
        if (isNull(this.doc.getSpecifiedLineTradeAgreement().getNetPriceProductTradePrice())) {
            this.doc.getSpecifiedLineTradeAgreement().setNetPriceProductTradePrice(new TradePriceType());
        }
        final AmountType amount = new AmountType();
        amount.setValue(netAmount);
        this.doc.getSpecifiedLineTradeAgreement().getNetPriceProductTradePrice().setChargeAmount(amount);
        return this;
    }

    public Position grossAmount(final BigDecimal grossAmount) {
        if (isNull(this.doc.getSpecifiedLineTradeAgreement())) {
            this.doc.setSpecifiedLineTradeAgreement(new LineTradeAgreementType());
        }
        if (isNull(this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice())) {
            this.doc.getSpecifiedLineTradeAgreement().setGrossPriceProductTradePrice(new TradePriceType());
        }
        final AmountType amount = new AmountType();
        amount.setValue(grossAmount);
        this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice().setChargeAmount(amount);
        return this;
    }

    public Position totalNetAmount(final BigDecimal totalNetAmount) {
        if (isNull(this.doc.getSpecifiedLineTradeSettlement())) {
            this.doc.setSpecifiedLineTradeSettlement(new LineTradeSettlementType());
        }
        if (isNull(this.doc.getSpecifiedLineTradeSettlement().getSpecifiedTradeSettlementLineMonetarySummation())) {
            this.doc.getSpecifiedLineTradeSettlement().setSpecifiedTradeSettlementLineMonetarySummation(new TradeSettlementLineMonetarySummationType());
        }
        final AmountType total = new AmountType();
        total.setValue(totalNetAmount);
        this.doc.getSpecifiedLineTradeSettlement().getSpecifiedTradeSettlementLineMonetarySummation().setLineTotalAmount(total);
        return this;
    }

    public Position quantity(final BigDecimal quantity, final Unit unit) {
        if (isNull(this.doc.getSpecifiedLineTradeAgreement())) {
            this.doc.setSpecifiedLineTradeAgreement(new LineTradeAgreementType());
        }
        if (isNull(this.doc.getSpecifiedLineTradeAgreement().getNetPriceProductTradePrice())) {
            this.doc.getSpecifiedLineTradeAgreement().setNetPriceProductTradePrice(new TradePriceType());
        }
        if (isNull(this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice())) {
            this.doc.getSpecifiedLineTradeAgreement().setGrossPriceProductTradePrice(new TradePriceType());
        }
        final QuantityType priceType = new QuantityType();
        priceType.setValue(BigDecimal.ONE);
        priceType.setUnitCode(unit.getCode());
        this.doc.getSpecifiedLineTradeAgreement().getNetPriceProductTradePrice().setBasisQuantity(priceType);
        this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice().setBasisQuantity(priceType);

        if (isNull(this.doc.getSpecifiedLineTradeDelivery())) {
            this.doc.setSpecifiedLineTradeDelivery(new LineTradeDeliveryType());
        }
        final QuantityType type = new QuantityType();
        type.setValue(quantity);
        type.setUnitCode(unit.getCode());
        this.doc.getSpecifiedLineTradeDelivery().setBilledQuantity(type);

        return this;
    }

    public Position taxes(final List<Tax> taxes) {
        if (isNull(this.doc.getSpecifiedLineTradeSettlement())) {
            this.doc.setSpecifiedLineTradeSettlement(new LineTradeSettlementType());
        }
        this.doc.getSpecifiedLineTradeSettlement().getApplicableTradeTaxes().clear();
        taxes.forEach(this::addTax);
        return this;
    }

    public Position addTax(final Tax tax) {
        if (isNull(this.doc.getSpecifiedLineTradeSettlement())) {
            this.doc.setSpecifiedLineTradeSettlement(new LineTradeSettlementType());
        }
        this.doc.getSpecifiedLineTradeSettlement().getApplicableTradeTaxes().add(tax.build());
        return this;
    }

    public Position discounts(final List<PositionDiscount> discounts) {
        if (isNull(this.doc.getSpecifiedLineTradeAgreement())) {
            this.doc.setSpecifiedLineTradeAgreement(new LineTradeAgreementType());
        }
        if (isNull(this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice())) {
            this.doc.getSpecifiedLineTradeAgreement().setGrossPriceProductTradePrice(new TradePriceType());
        }
        this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice().getAppliedTradeAllowanceCharges().clear();
        discounts.forEach(this::addDiscount);
        return this;
    }

    public Position addDiscount(final PositionDiscount discount) {
        if (isNull(this.doc.getSpecifiedLineTradeAgreement())) {
            this.doc.setSpecifiedLineTradeAgreement(new LineTradeAgreementType());
        }
        if (isNull(this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice())) {
            this.doc.getSpecifiedLineTradeAgreement().setGrossPriceProductTradePrice(new TradePriceType());
        }
        this.doc.getSpecifiedLineTradeAgreement().getGrossPriceProductTradePrice().getAppliedTradeAllowanceCharges().add(discount.build());
        return this;
    }
}
