package one.epicsoft.zugferd.builders;

import java.math.BigDecimal;

import one.epicsoft.zugferd.extended.AmountType;
import one.epicsoft.zugferd.extended.PercentType;
import one.epicsoft.zugferd.extended.TaxCategoryCodeContentType;
import one.epicsoft.zugferd.extended.TaxCategoryCodeType;
import one.epicsoft.zugferd.extended.TaxTypeCodeContentType;
import one.epicsoft.zugferd.extended.TaxTypeCodeType;
import one.epicsoft.zugferd.extended.TradeTaxType;

public class Tax {

    private final TradeTaxType doc;

    private Tax() {
        this.doc = new TradeTaxType();
        this.code(TaxTypeCodeContentType.VAT);
    }

    public static Tax builder() {
        return new Tax();
    }

    public TradeTaxType build() {
        return this.doc;
    }

    public Tax calculatedAmount(final BigDecimal calculatedAmount) {
        final AmountType value = new AmountType();
        value.setValue(calculatedAmount);
        this.doc.setCalculatedAmount(value);
        return this;
    }

    public Tax basisAmount(final BigDecimal basisAmount) {
        final AmountType value = new AmountType();
        value.setValue(basisAmount);
        this.doc.setBasisAmount(value);
        return this;
    }

    public Tax rate(final TaxCode code, final BigDecimal ratePercent) {
        final TaxCategoryCodeType category = new TaxCategoryCodeType();
        category.setValue(this.getTaxCategory(code));

        final PercentType rate = new PercentType();
        rate.setValue(ratePercent);

        this.doc.setCategoryCode(category);
        this.doc.setRateApplicablePercent(rate);
        return this;
    }

    public Tax code(final TaxTypeCodeContentType code) {
        final TaxTypeCodeType taxCode = new TaxTypeCodeType();
        taxCode.setValue(code);
        this.doc.setTypeCode(taxCode);
        return this;
    }

    public Tax categoryCode(final TaxCode categoryCode) {
        return this.categoryCode(TaxCategoryCodeContentType.valueOf(categoryCode.getCategoryCode()));
    }

    public Tax categoryCode(final TaxCategoryCodeContentType categoryCode) {
        final TaxCategoryCodeType value = new TaxCategoryCodeType();
        value.setValue(categoryCode);
        this.doc.setCategoryCode(value);
        return this;
    }

    private TaxCategoryCodeContentType getTaxCategory(final TaxCode taxCode) {
        for (final TaxCategoryCodeContentType type : TaxCategoryCodeContentType.values()) {
            if (type.name().equalsIgnoreCase(taxCode.getCategoryCode())) {
                return type;
            }
        }
        throw new IllegalArgumentException(String.format("TaxCategoryCodeContentType not exists for TaxCode '%s'", taxCode));
    }
}
