package one.epicsoft.zugferd.builders;

import java.math.BigDecimal;

import one.epicsoft.zugferd.extended.AmountType;
import one.epicsoft.zugferd.extended.IndicatorType;
import one.epicsoft.zugferd.extended.PercentType;
import one.epicsoft.zugferd.extended.TextType;
import one.epicsoft.zugferd.extended.TradeAllowanceChargeType;

public class PositionDiscount {

    private static final boolean ALLOWANCE = true;

    private final TradeAllowanceChargeType doc;

    /**
     * EN 16931: false = allowance
     * Extended: false = allowance and true = charge
     */
    private PositionDiscount() {
        this.doc = new TradeAllowanceChargeType();
        this.doc.setChargeIndicator(new IndicatorType());
        this.doc.getChargeIndicator().setIndicator(ALLOWANCE);
    }

    public static PositionDiscount builder() {
        return new PositionDiscount();
    }

    public TradeAllowanceChargeType build() {
        return this.doc;
    }

    public PositionDiscount percent(final BigDecimal percent) {
        final PercentType value = new PercentType();
        value.setValue(percent);
        this.doc.setCalculationPercent(value);
        return this;
    }

    public PositionDiscount netAmount(final BigDecimal netAmount) {
        final AmountType value = new AmountType();
        value.setValue(netAmount);
        this.doc.setBasisAmount(value);
        return this;
    }

    /**
     * Description: The total discount substracted from the gross price which leads to the net price
     * Note: This is only true where the discount is based on items and not included in the gross price.
     * Synonym: Amount of the charge / discount
     */
    public PositionDiscount totalNetAmount(final BigDecimal totalNetAmount) {
        final AmountType value = new AmountType();
        value.setValue(totalNetAmount);
        this.doc.setActualAmount(value);
        return this;
    }

    public PositionDiscount reason(final String reason) {
        final TextType value = new TextType();
        value.setValue(reason);
        this.doc.setReason(value);
        return this;
    }
}
