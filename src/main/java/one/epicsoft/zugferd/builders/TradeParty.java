package one.epicsoft.zugferd.builders;

import static java.util.Objects.isNull;
import static one.epicsoft.zugferd.helpers.StringUtils.emptyIfBlank;
import static one.epicsoft.zugferd.helpers.StringUtils.hasText;

import one.epicsoft.zugferd.extended.CodeType;
import one.epicsoft.zugferd.extended.CountryIDType;
import one.epicsoft.zugferd.extended.TextType;
import one.epicsoft.zugferd.extended.TradeAddressType;
import one.epicsoft.zugferd.extended.TradePartyType;

public abstract class TradeParty {

    protected TradePartyType doc;

    public TradePartyType build() {
        return this.doc;
    }

    public TradeParty name(final String name) {
        final TextType text = new TextType();
        text.setValue(emptyIfBlank(name));
        this.doc.setName(text);
        return this;
    }

    public TradeParty zipCode(final String zipCode) {
        if (isNull(this.doc.getPostalTradeAddress())) {
            this.doc.setPostalTradeAddress(new TradeAddressType());
        }
        final CodeType code = new CodeType();
        code.setValue(zipCode);
        this.doc.getPostalTradeAddress().setPostcodeCode(code);
        return this;
    }

    public TradeParty streetWithHouseNumber(final String streetWithHouseNo) {
        if (isNull(this.doc.getPostalTradeAddress())) {
            this.doc.setPostalTradeAddress(new TradeAddressType());
        }
        final TextType text = new TextType();
        text.setValue(emptyIfBlank(streetWithHouseNo));
        this.doc.getPostalTradeAddress().setLineOne(text);
        return this;
    }

    public TradeParty city(final String city) {
        if (isNull(this.doc.getPostalTradeAddress())) {
            this.doc.setPostalTradeAddress(new TradeAddressType());
        }
        final TextType text = new TextType();
        text.setValue(emptyIfBlank(city));
        this.doc.getPostalTradeAddress().setCityName(text);
        return this;
    }

    public TradeParty countryIso2(final String countryIso2) {
        if (hasText(countryIso2) && countryIso2.length() == 2) {
            if (isNull(this.doc.getPostalTradeAddress())) {
                this.doc.setPostalTradeAddress(new TradeAddressType());
            }
            final CountryIDType id = new CountryIDType();
            id.setValue(countryIso2);
            this.doc.getPostalTradeAddress().setCountryID(id);
            return this;
        } else {
            throw new IllegalArgumentException(String.format("Country ISO code 2: %s", countryIso2));
        }
    }
}
