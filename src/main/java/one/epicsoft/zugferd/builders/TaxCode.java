package one.epicsoft.zugferd.builders;

/**
 * Source: https://unece.org/fileadmin/DAM/trade/untdid/d16b/tred/tred5305.htm
 */
public enum TaxCode {

    // Mixed tax rate
    MIXED_TAX_RATE("Code specifying that the rate is based on mixed tax", "A"),

    // Lower rate
    LOWER_RATE("Tax rate is lower than standard rate", "AA"),

    // Exempt for resale
    EXEMPT_FOR_RESALE("A tax category code indicating the item is tax exempt when the item is bought for future resale", "AB"),

    // Value Added Tax (VAT) not now due for payment
    VALUE_ADDED_TAX("A code to indicate that the Value Added Tax (VAT) amount which is due on the current invoice is to be paid on receipt of a separate VAT payment request",
            "AC"),

    // Value Added Tax (VAT) due from a previous invoice
    VALUE_ADDED_TAX_PREVIOUS_INVOICE("A code to indicate that the Value Added Tax (VAT) amount of a previous invoice is to be paid", "AD"),

    // VAT Reverse Charge
    REVERSE_CHANGE("Code specifying that the standard VAT rate is levied from the invoicee", "AE"),

    // Transferred (VAT)
    TRANSFERRED("VAT not to be paid to the issuer of the invoice but directly to relevant tax authority.", "B"),

    // Duty paid by supplier
    DUTY_PAID("Duty associated with shipment of goods is paid by the supplier; customer receives goods with duty paid.", "C"),

    // Value Added Tax (VAT) margin scheme - travel agents
    VALUE_ADDED_TAX_MARGIN_TRAVEL("Indication that the VAT margin scheme for travel agents is applied.", "D"),

    // Exempt from tax
    EXEMPT("Code specifying that taxes are not applicable", "E"),

    // Value Added Tax (VAT) margin scheme - second-hand goods
    VALUE_ADDED_TAX_MARGIN_SECOND_HAND("Indication that the VAT margin scheme for second-hand goods is applied", "F"),

    // Free export item, tax not charged
    FREE_EXPORT("Code specifying that the item is free export and taxes are not charged", "G"),

    // Higher rate
    HIGHER_RATE("Code specifying a higher rate of duty or tax or fee", "H"),

    // Value Added Tax (VAT) margin scheme - works of art
    VALUE_ADDED_TAX_MARGIN_ART("Margin scheme Works of art Indication that the VAT margin scheme for works of art is applied", "I"),

    // Value Added Tax (VAT) margin scheme - collector's items and antiques
    VALUE_ADDED_TAX_MARGIN_ANTIQUES("Indication that the VAT margin scheme for collector's items and antiques is applied", "J"),

    // VAT exempt for EEA intra-community supply of goods and
    INTRA_COMMUNITY("A tax category code indicating the item is VAT exempt due to an intra-community supply in the European Economic Area", "K", true),

    // Canary Islands general indirect tax
    CANARY_ISLAND_INDIRECT("Impuesto General Indirecto Canario (IGIC) is an indirect tax levied on goods and services supplied in the Canary Islands (Spain) "
            + "by traders and professionals, as well as on import of goods", "L", true),

    // Tax for production, services and importation in Ceuta and Melilla
    CEUTA_MELILLA_IMPORT("Impuesto sobre la Produccin, los Servicios y la Importaci�n (IPSI) is an indirect municipal tax, levied on the production, "
            + "processing and import of all kinds of movable tangible property, "
            + "the supply of services and the transfer of immovable property located in the cities of Ceuta and Melilla", "M", true),

    // Services outside scope of tax
    OUTSIDE_SCOPE("Code specifying that taxes are not applicable to the services.", "O"),

    // Standard rate
    STANDARD_RATE("Code specifying the standard rate", "S"),

    // Zero rated goods
    ZERO_RATED_GOODS("Code specifying that the goods are at a zero rate", "Z");

    private String description;

    private String categoryCode;

    private boolean addition;

    TaxCode(final String description, final String categoryCode) {
        this(description, categoryCode, false);
    }

    TaxCode(final String description, final String categoryCode, final boolean addition) {
        this.description = description;
        this.categoryCode = categoryCode;
        this.addition = addition;
    }

    public String getDescription() {
        return this.description;
    }

    public String getCategoryCode() {
        return this.categoryCode;
    }

    public boolean isAddition() {
        return this.addition;
    }
}
