package one.epicsoft.zugferd.builders;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class DateTime {

    private final TemporalAccessor temporal;

    private DateTime(final TemporalAccessor temporal) {
        this.temporal = temporal;
    }

    public static DateTime of(final OffsetDateTime date) {
        return new DateTime(date);
    }

    public static DateTime of(final LocalDate date) {
        return new DateTime(date);
    }

    public String format(final DateTimeFormat format) {
        return DateTimeFormatter.ofPattern(format.getPattern()).format(this.temporal);
    }
}
