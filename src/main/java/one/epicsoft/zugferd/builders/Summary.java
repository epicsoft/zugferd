package one.epicsoft.zugferd.builders;

import static java.util.Objects.isNull;
import static one.epicsoft.zugferd.helpers.StringUtils.emptyIfBlank;

import java.math.BigDecimal;
import java.util.List;

import one.epicsoft.zugferd.extended.AmountType;
import one.epicsoft.zugferd.extended.CurrencyCodeContentType;
import one.epicsoft.zugferd.extended.CurrencyCodeType;
import one.epicsoft.zugferd.extended.DateTimeType;
import one.epicsoft.zugferd.extended.DateTimeType.DateTimeString;
import one.epicsoft.zugferd.extended.HeaderTradeSettlementType;
import one.epicsoft.zugferd.extended.TextType;
import one.epicsoft.zugferd.extended.TradePaymentTermsType;
import one.epicsoft.zugferd.extended.TradeSettlementHeaderMonetarySummationType;

public class Summary {

    private final HeaderTradeSettlementType doc;

    private Summary() {
        this.doc = new HeaderTradeSettlementType();
    }

    public static Summary builder() {
        return new Summary();
    }

    public HeaderTradeSettlementType build() {
        return this.doc;
    }

    public Summary paymentReference(final String paymentReference) {
        final TextType text = new TextType();
        text.setValue(emptyIfBlank(paymentReference));
        this.doc.setPaymentReference(text);
        return this;
    }

    public Summary currency(final Currency currency) {
        final CurrencyCodeType code = new CurrencyCodeType();
        code.setValue(this.getCurrencyCode(currency));
        this.doc.setInvoiceCurrencyCode(code);
        return this;
    }

    public Summary taxes(final List<Tax> taxes) {
        this.doc.getApplicableTradeTaxes().clear();
        taxes.forEach(this::addTax);
        return this;
    }

    public Summary addTax(final Tax tax) {
        this.doc.getApplicableTradeTaxes().add(tax.build());
        return this;
    }

    public Summary dueDate(final DateTime dueDate) {
        final DateTimeString date = new DateTimeString();
        date.setFormat(Invoice.DATE_FORMAT.getName());
        date.setValue(dueDate.format(Invoice.DATE_FORMAT));

        final DateTimeType dateType = new DateTimeType();
        dateType.setDateTimeString(date);

        final TradePaymentTermsType paymentTerm = new TradePaymentTermsType();
        paymentTerm.setDueDateDateTime(dateType);

        this.doc.getSpecifiedTradePaymentTerms().clear();
        this.doc.getSpecifiedTradePaymentTerms().add(paymentTerm);
        return this;
    }

    public Summary lineTotalAmount(final BigDecimal lineTotalAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(lineTotalAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().setLineTotalAmount(value);
        return this;
    }

    public Summary chargeTotalAmount(final BigDecimal chargeTotalAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(chargeTotalAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().setChargeTotalAmount(value);
        return this;
    }

    public Summary allowanceTotalAmount(final BigDecimal allowanceTotalAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(allowanceTotalAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().setAllowanceTotalAmount(value);
        return this;
    }

    public Summary taxBasisTotalAmount(final BigDecimal taxBasisTotalAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(taxBasisTotalAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().getTaxBasisTotalAmounts().clear();
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().getTaxBasisTotalAmounts().add(value);
        return this;
    }

    public Summary taxTotalAmount(final BigDecimal taxTotalAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(taxTotalAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().getTaxTotalAmounts().clear();
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().getTaxTotalAmounts().add(value);
        return this;
    }

    public Summary grandTotalAmount(final BigDecimal grandTotalAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(grandTotalAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().getGrandTotalAmounts().clear();
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().getGrandTotalAmounts().add(value);
        return this;
    }

    public Summary totalPrepaidAmount(final BigDecimal totalPrepaidAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(totalPrepaidAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().setTotalPrepaidAmount(value);
        return this;
    }

    public Summary duePayableAmount(final BigDecimal duePayableAmount) {
        if (isNull(this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation())) {
            this.doc.setSpecifiedTradeSettlementHeaderMonetarySummation(new TradeSettlementHeaderMonetarySummationType());
        }
        final AmountType value = new AmountType();
        value.setValue(duePayableAmount);
        this.doc.getSpecifiedTradeSettlementHeaderMonetarySummation().setDuePayableAmount(value);
        return this;
    }

    private CurrencyCodeContentType getCurrencyCode(final Currency currency) {
        return CurrencyCodeContentType.valueOf(currency.name());
    }
}
