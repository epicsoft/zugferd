package one.epicsoft.zugferd.builders;

public enum DateTimeFormat {

    // 102 CCYYMMDD
    F_102("102", "yyyyMMdd");

    private String name;

    private String pattern;

    DateTimeFormat(final String name, final String pattern) {
        this.name = name;
        this.pattern = pattern;
    }

    public String getName() {
        return this.name;
    }

    public String getPattern() {
        return this.pattern;
    }
}
