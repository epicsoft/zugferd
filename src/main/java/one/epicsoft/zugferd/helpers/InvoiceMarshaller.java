package one.epicsoft.zugferd.helpers;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UncheckedIOException;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import one.epicsoft.zugferd.builders.Invoice;
import one.epicsoft.zugferd.exceptions.UncheckedJAXBException;
import one.epicsoft.zugferd.extended.CrossIndustryInvoice;

public class InvoiceMarshaller {

    private InvoiceMarshaller() {
        // Do not init
    }

    public static String marshal(final Invoice invoice) {
        return marshal(invoice.buildCrossIndustryInvoice());
    }

    public static String marshal(final CrossIndustryInvoice invoice) {
        try (StringWriter writer = new StringWriter()) {
            JAXBContext.newInstance(CrossIndustryInvoice.class).createMarshaller().marshal(invoice, writer);
            return writer.toString();
        } catch (final JAXBException e) {
            throw new UncheckedJAXBException(e);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
