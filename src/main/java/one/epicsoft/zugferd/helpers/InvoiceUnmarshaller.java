package one.epicsoft.zugferd.helpers;

import java.io.StringReader;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import one.epicsoft.zugferd.exceptions.UncheckedJAXBException;
import one.epicsoft.zugferd.extended.CrossIndustryInvoice;

public class InvoiceUnmarshaller {

    private InvoiceUnmarshaller() {
        // Do not init
    }

    public static CrossIndustryInvoice unmarshal(final String xmlContent) {
        try (final StringReader reader = new StringReader(xmlContent)) {
            final JAXBContext jaxbContext = JAXBContext.newInstance(CrossIndustryInvoice.class);
            final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (CrossIndustryInvoice) jaxbUnmarshaller.unmarshal(reader);
        } catch (final JAXBException e) {
            throw new UncheckedJAXBException(e);
        }
    }
}
