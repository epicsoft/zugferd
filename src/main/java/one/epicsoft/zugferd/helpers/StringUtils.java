package one.epicsoft.zugferd.helpers;

import static java.util.Objects.nonNull;

public class StringUtils {

    private StringUtils() {
        // do not init
    }

    public static boolean hasText(final String in) {
        return nonNull(in) && !in.isBlank();
    }

    public static String emptyIfBlank(final String in) {
        return hasText(in) ? in : "";
    }
}
