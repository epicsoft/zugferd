package one.epicsoft.zugferd.helpers;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.junit.jupiter.api.Test;

import one.epicsoft.zugferd.builders.DateTime;
import one.epicsoft.zugferd.builders.Invoice;
import one.epicsoft.zugferd.builders.InvoiceType;
import one.epicsoft.zugferd.extended.CrossIndustryInvoice;
import one.epicsoft.zugferd.extended.DocumentContextParameterType;
import one.epicsoft.zugferd.extended.ExchangedDocumentContextType;
import one.epicsoft.zugferd.extended.IDType;

class InvoiceMarshallerTest {

    @Test
    void marshal_withCrossIndustryInvoiceObject_toXmlInvoice() {
        // GIVEN
        final IDType idType = new IDType();
        idType.setValue("urn:factur-x.eu:1p0:custom");
        final DocumentContextParameterType guidelineSpecifiedDocumentContextParameter = new DocumentContextParameterType();
        guidelineSpecifiedDocumentContextParameter.setID(idType);
        final ExchangedDocumentContextType exchangedDocumentContext = new ExchangedDocumentContextType();
        exchangedDocumentContext.setGuidelineSpecifiedDocumentContextParameter(guidelineSpecifiedDocumentContextParameter);
        final CrossIndustryInvoice invoice = new CrossIndustryInvoice();
        invoice.setExchangedDocumentContext(exchangedDocumentContext);
        // WHEN
        final String result = InvoiceMarshaller.marshal(invoice);
        // THEN
        assertThat(result).isNotNull()
                .containsIgnoringCase("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>")
                .contains("urn:factur-x.eu:1p0:custom");
    }

    @Test
    void marshal_withInvoiceObject_toXmlInvoice() {
        // GIVEN
        final OffsetDateTime issueDate = OffsetDateTime.of(2021, 8, 24, 0, 0, 0, 0, ZoneOffset.UTC);
        final Invoice invoice = Invoice.builder()
                .specificationIdentifier("urn:factur-x.eu:1p0:custom")
                .invoiceNumber("123456789")
                .type(InvoiceType.INVOICE)
                .issueDate(DateTime.of(issueDate));
        // WHEN
        final String result = InvoiceMarshaller.marshal(invoice);
        // THEN
        assertThat(result).isNotNull()
                .containsIgnoringCase("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>")
                .contains("<ns2:ID>urn:factur-x.eu:1p0:custom</ns2:ID>")
                .contains("<ns2:ID>123456789</ns2:ID>")
                .contains("<ns2:TypeCode>380</ns2:TypeCode>")
                .contains("<DateTimeString format=\"102\">20210824</DateTimeString>");
    }
}
