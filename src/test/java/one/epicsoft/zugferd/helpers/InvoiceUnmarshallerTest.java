package one.epicsoft.zugferd.helpers;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import one.epicsoft.zugferd.extended.CrossIndustryInvoice;

class InvoiceUnmarshallerTest {

    @ParameterizedTest
    @MethodSource("provideArgsForUnmarshal")
    void unmarshal_withXmlInvoice_toCrossIndustryInvoiceObject(final String xmlPath, final String expectedId) throws IOException {
        // GIVEN
        final String xmlContent = new String(this.getClass().getClassLoader().getResourceAsStream(xmlPath).readAllBytes());
        // WHEN
        final CrossIndustryInvoice result = InvoiceUnmarshaller.unmarshal(xmlContent);
        // THEN
        assertThat(result).isNotNull();
        // AND - ExchangedDocumentContext
        assertThat(result.getExchangedDocumentContext()).isNotNull();
        assertThat(result.getExchangedDocumentContext().getGuidelineSpecifiedDocumentContextParameter()).isNotNull();
        assertThat(result.getExchangedDocumentContext().getGuidelineSpecifiedDocumentContextParameter().getID()).isNotNull();
        assertThat(result.getExchangedDocumentContext().getGuidelineSpecifiedDocumentContextParameter().getID().getValue()).isEqualTo(expectedId);
    }

    private static Stream<Arguments> provideArgsForUnmarshal() {
        return Stream.of(
                Arguments.of("ZUGFeRD22/Samples/MINIMUM/zugferd_2p1_MINIMUM_Rechnung.xml", "urn:factur-x.eu:1p0:minimum"),
                Arguments.of("ZUGFeRD22/Samples/BASIC/zugferd_2p1_BASIC_Einfach.xml", "urn:cen.eu:en16931:2017#compliant#urn:factur-x.eu:1p0:basic"),
                Arguments.of("ZUGFeRD22/Samples/EXTENDED/zugferd_2p1_EXTENDED_Fremdwaehrung.xml", "urn:cen.eu:en16931:2017#conformant#urn:factur-x.eu:1p0:extended"));
    }
}
